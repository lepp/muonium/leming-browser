import ROOT
import string
import tempfile
import time
import re

default_data_location = "/data0/leming/currentBT/his/off"  # "/home/rebecca/Downloads"


class InvalidRunSpecification(Exception):
    pass


class LDlist:
    def __init__(self, *args):
        self.data_list = []
        for arg in args:
            ld = LemingData(arg)
            self.data_list.append(ld)
            first_file = args[0]


class LemingData:
    file = None
    name = None
    objs = dict()
    cans = dict()
    stacks = dict()

    def __init__(self, file_or_runspec_list):
        self.open(file_or_runspec_list)

    def clear(self):
        self.objs.clear()
        self.cans.clear()
        self.stacks.clear()

        if self.file is not None:
            self.file.Close()

    def clear_canv(self, name):
        if name not in self.cans:
            self.cans[name] = []
        self.cans[name].clear()

    def clear_stack(self, name):
        if name not in self.stacks:
            self.stacks[name] = []
        self.stacks[name].clear()

    def clear_objs(self, dict_name):
        if dict_name not in self.objs:
            self.objs[dict_name] = []

        self.objs[dict_name].clear()

    def open(self, file_or_runspec_list):
        self.clear()
        path = file_or_runspec_list[0]
        file_or_runspec = file_or_runspec_list[1]
        self.name = file_or_runspec_list[2]
        if ".root" in file_or_runspec and path != "":
            self.file = ROOT.TFile(path + "/" + file_or_runspec, "READ")
        elif ".root" in file_or_runspec and path == "":
            self.file = ROOT.TFile(
                default_data_location + "/" + file_or_runspec, "READ"
            )
        else:
            file_list = self.__get_files_from_runspec(path, file_or_runspec)
            merger = ROOT.TFileMerger()
            for f in file_list:
                merger.AddFile(f)
            with tempfile.NamedTemporaryFile() as tmp:
                merger.OutputFile(tmp.name)
                merger.Merge()
                # See https://www.gnu.org/software/libc/manual/html_node/Deleting-Files.html for how this works
                self.file = ROOT.TFile(tmp.name, "READ")

    def get_object(self, name):
        obj = self.file.Get(name)
        return obj

    def get_name(self):
        if (
            (".root" in self.name)
            and ("-" not in self.name)
            and not (any(char.isalpha() for char in self.name))
        ):
            match = re.match(r"^.*\/output0(.*)\.root.*$", self.name)
            return match.group(1)
        elif (".root" in self.name) and (
            ("-" in self.name) or (any(char.isalpha() for char in self.name))
        ):
            text = self.name[:-5]
            return text
        else:
            return self.name

    # Make sure that object/canvas name has not been used yet when keeping object/canvas!
    def keep_object(self, obj, name):
        if name not in self.objs:
            self.objs[name] = []
        self.objs[name].append(obj)

    def keep_canvas(self, canvas, name):
        if name not in self.cans:
            self.cans[name] = []
        self.cans[name].append(canvas)

    def keep_stack(self, stack, name):
        if name not in self.stacks:
            self.stacks[name] = []
        self.stacks[name].append(stack)

    def get_clone(self, name, dict_name):
        if dict_name not in self.objs:
            self.objs[dict_name] = []
        clone = self.file.Get(name).Clone(f"{dict_name}_{len(self.objs[dict_name])}")
        self.objs[dict_name].append(clone)
        return clone

    def get_hist_from_tree(self, histName, dict_name, Ecut_low):
        if dict_name not in self.objs:
            self.objs[dict_name] = []

        tCoincidencePP = self.file.Get("tCoincidencePP")

        draw_command = f"{histName}_t[0]>>{histName}(200,-10000,10000)"
        print("draw command", draw_command)
        cut_condition = (
            f"{histName}_E[0]>{Ecut_low[0]} && {histName}_E[1]>{Ecut_low[1]}"
        )
        print("cut_condition", cut_condition)
        nentries = tCoincidencePP.Draw(draw_command, cut_condition)
        histogram = ROOT.gPad.GetPrimitive(f"{histName}")
        self.objs[dict_name].append(histogram)

        return histogram

    def get_hist_from_tree_triple_coin_N(self, histName, dict_name, Ecut_low):
        if dict_name not in self.objs:
            self.objs[dict_name] = []

        tCoincidencePP = self.file.Get("tCoincidencePP")

        draw_command = f"{histName}_E[2]>>{histName}(1000,0,2000)"
        print("draw command", draw_command)
        cut_condition = f"{histName}_E[1]>{histName}_E[2] && {histName}_E[0]>{Ecut_low[0]} && {histName}_E[1]>{Ecut_low[1]}"
        print("cut_condition", cut_condition)
        nentries = tCoincidencePP.Draw(draw_command, cut_condition)
        histogram = ROOT.gPad.GetPrimitive(f"{histName}")
        self.objs[dict_name].append(histogram)

        return histogram

    def get_hist_from_tree_triple_coin_F(self, histName, dict_name, Ecut_low):
        if dict_name not in self.objs:
            self.objs[dict_name] = []

        tCoincidencePP = self.file.Get("tCoincidencePP")

        draw_command = f"{histName}_E[2]>>{histName}(1000,0,2000)"
        print("draw command", draw_command)
        cut_condition = f"{histName}_E[0]>{histName}_E[2] && {histName}_E[0]>{Ecut_low[0]} && {histName}_E[1]>{Ecut_low[1]}"
        print("cut_condition", cut_condition)
        nentries = tCoincidencePP.Draw(draw_command, cut_condition)
        histogram = ROOT.gPad.GetPrimitive(f"{histName}")
        self.objs[dict_name].append(histogram)

        return histogram

    def get_projx(self, name, dict_name, first_bin, last_bin):
        if dict_name not in self.objs:
            self.objs[dict_name] = []
        proj = self.file.Get(name).ProjectionX(
            f"{dict_name}_px_{len(self.objs[dict_name])}", first_bin, last_bin
        )
        self.objs[dict_name].append(proj)
        return proj

    def get_projy(self, name, dict_name, first_bin, last_bin):
        if dict_name not in self.objs:
            self.objs[dict_name] = []
        proj = self.file.Get(name).ProjectionY(
            f"{dict_name}_py_{len(self.objs[dict_name])}", first_bin, last_bin
        )
        self.objs[dict_name].append(proj)
        return proj

    def create_canvas(self, name, title, *args, **kwargs):
        # create canvas, add canvas to dictionary. Dictionary section defined by name given to canvas
        if name not in self.cans:
            self.cans[name] = []
        canvas = ROOT.TCanvas(
            f"cLEMING_{name}_{len(self.cans[name])}", title, *args, **kwargs
        )
        self.cans[name].append(canvas)

        # Wait for the browser tab to open.
        # Otherwise opening multiple canvases rapidly fails.
        time.sleep(1)
        return canvas

    def create_stack(self, name, title, *args, **kwargs):
        # create stack, add stack to dictionary. Dictionary section defined by name given to stack
        if name not in self.stacks:
            self.stacks[name] = []
        stack = ROOT.THStack(
            f"sLEMING_{name}_{len(self.stacks[name])}", title, *args, **kwargs
        )
        self.stacks[name].append(stack)

        return stack

    def create_legend(self, name, title, *args, **kwargs):
        # create legend, add legend to dictionary. Dictionary section defined by name given to legend
        if name not in self.cans:
            self.cans[name] = []
        legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
        self.cans[name].append(legend)

        return legend

    def create_stupid_tline(self, name, title, ecut, color, *args, **kwargs):
        # create line, add line to dictionary. Dictionary section defined by name given to line
        if name not in self.cans:
            self.cans[name] = []
        Tline = ROOT.TLine(
            ecut, 0, ecut, 100
        )  # height is hardcoded, feel free to make it nice, but it's 4 am and I dont give enough shits anymore
        Tline.SetLineWidth(1)
        Tline.SetLineColor(color)
        self.cans[name].append(Tline)

        return Tline

    def __validate_runspec(self, runspec):
        valid_chars = set(string.digits + "," + "-")
        if not set(runspec).issubset(valid_chars):
            raise InvalidRunSpecification(runspec)

    def __get_file_from_runid(self, path, runid):
        if path == "":
            return f"{default_data_location}/output{runid:05d}.root"
        else:
            return path + f"/output{runid:05d}.root"

    def __get_files_from_runspec(self, path, runspec):
        file_list = []
        runspec = "".join(runspec.split())  # Remove whitespace
        self.__validate_runspec(runspec)
        for rs in runspec.split(","):
            if rs == "":
                raise InvalidRunSpecification(runspec)
            run_range = []
            for run in rs.split("-"):
                if run == "":
                    raise InvalidRunSpecification(runspec)
                run_range.append(int(run))
            if len(run_range) == 1:
                run_range.append(run_range[0])
            if len(run_range) != 2:
                raise InvalidRunSpecification(runspec)
            elif run_range[0] > run_range[1]:
                raise InvalidRunSpecification(runspec)
            run_range[1] += 1
            for runid in range(*run_range):
                file_list.append(self.__get_file_from_runid(path, runid))
        return file_list
