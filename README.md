# LEMING Browser



## Description
This application can be used to draw multiple histograms that are useful for ad-hoc data analysis for example during data taking in a beamtime. 
Severa histograms are available:

Muon Energy: Opens hMuonEnergy and displays it as a projection onto the x-
axis.

Electron Energy Left: Opens hElectronEnergy and displays all lines that correspond to
the left detectors as individual projections onto the x-axis.

Electron Energy Right: Opens hElectronEnergy and displays all lines that correspond to
the right detectors as individual projections onto the x-axis.

Electron Energy Bottom: Opens hElectronEnergy and displays all lines that correspond to
the bottom detectors as individual projections onto the x-axis.

Electron Time Left: Opens hElectronVsTime and displays all lines that correspond to
the left detectors as individual projections onto the x-axis.

Electron Time Right: Opens hElectronVsTime and displays all lines that correspond to
the right detectors as individual projections onto the x-axis.

Coin Left: Opens hCoincidencePP L F1aN1, hCoincidencePP L F2aN2,
hCoincidencePP L F3aN3, and hCoincidencePP L F4aN4 and
displays these histograms.

Coin Left, Tcorr: Opens the same histograms as Coin Left and applies a lifetime
correction before displaying.

Coin Left, Tcorr, BgSub: Opens the same histograms as Coin Left, applies a lifetime
correction and background subtraction.

Coin Right: Opens hCoincidencePP R F1aN1, hCoincidencePP R F2aN2, hCoincidencePP R F3aN3, 
and hCoincidencePP R F4aN4 and displays these histograms.

Coin Right, Tcorr: Opens the same histograms as Coin Right and applies a lifetime
correction before displaying.

Coin Right, Tcorr, BgSub: Opens the same histograms as Coin Right and applies a lifetime
correction and background subtraction.

Coin Bottom: Opens hCoincidencePP L L1aH1, hCoincidencePP L L2aH2, hCoincidencePP L L3aH3, 
hCoincidencePP L L1aH2, hCoincidencePP L L2aH3, hCoincidencePP L L1aH3, 
hCoincidencePP L L2aH1, hCoincidencePP L L3aH2 and hCoincidencePP L L3aH1 and displays these histograms.

Coin Bottom, Tcorr: Opens the same histograms as Coin Bottom and applies a lifetime correction
before displaying.

Coin Bottom, Tcorr, BgSub: Opens the same histograms as Coin Bottom, applies a lifetime correction and
background subtraction.

Coin LR: Opens the histograms of Coin Left and Coin Right and adds them before
displaying.

Coin LR, Tcorr: Opens the histograms of Coin Left and Coin Right, applies a lifetime correction
and adds them.

Coin LR, Tcorr, BgSub: Opens the histograms of Coin Left and Coin Right, applies a lifetime correction,
a background subtraction and adds them.

Coin LR Gumbel Fit: Same procedure as Coin LR, Tcorr, BgSub but now with a Gumbel fit done.

Coin Vert: Opens hCoincidencePP L FAaNA, hCoincidencePP L FBaNB, hCoincidencePP L FCaNC, hCoincidencePP R FAaNA,
hCoincidencePP R FBaNB and hCoincidencePP R FCaNC and displays them.

Coin Vert, Tcorr: Opens the same histograms as Coin Vert and applies a lifetime correction before
displaying them.

General remarks about the analysis functions: 
The different functions and their respective histograms can be grouped into two categories: energy
spectra and time spectra. The energy spectra depict the deposited energy in the detectors and the
time spectra show the timing of the coincidences of the ”Far” and ”Near” detectors. Nevertheless there
are some features that all functions possess. The first being the clearing of all canvasses and objects
of the function’s respective dictionary key. All created canvasses or objects get saved into an object-
or canvas-specific dictionary when using the LemingData functions create_canvas(name, title),
get_clone(name, dictionary_name), get_projx(name, dictionary_name, first_bin, last_bin),
or get_projy(name, dictionary_name, first_bin, last_bin). They all also contain a list of colours
for the case when histograms from different ROOT files are displayed in the same pad. This is made
possible by the fact that the analysis functions receive the list of ROOT files and loop over them to
display the same histogram from different files together. Some of the functions also contain a scaling
factor based on the amount of hits registered to make comparisons possible.

Fitting functions with the present version of the online-display: 
In the present version of the online-display, the analysis functions coincidence_LR_added(root_file)
and coincidence_gumbel_fit(root_file), which correspond to the buttons ”Coin LR” and ”Coin LR
Gumbel Fit”, respectively, both perform fits on the data. In the case of coincidence_LR_added(root_file),
the data shows the coincidences of electrons in the ”Far” and ”Near” detectors in respect to the time
difference from the start of measurement ∆t. These electrons are the decay products of the muon in
muonium. Therefore the fit is that of an exponential decay with muon lifetime of τ_µ = 2.193µs and fit
parameter A:
n_coin = A ∗ exp(−∆t/τ_µ) (5)
The fit is done within the coincidene_LR_added(root_file) function, which means that the start
parameters for A are hardcoded to a number that works for most of the fits, which is 10. A similar
procedure is used for the coincidence_gumbel_fit(root_file) function. It displays the same histograms
as coincidence_LR_added(root_file) but including a lifetime and background correction. This allows
for a Gumbel fit to be made. Different than the function before, it has a more flexible way to determine
the starting parameters. First, a Gauss fit is done to determine the center of the peak. The result of
this fit is then used to give the peak center starting parameter of the Gumbel fit, which is more accurate
than setting a fixed value in the code.
There are also other ways to fit a user-defined function. When the web browser with the specific
histogram is opened, the Fit Panel can be found in the top menu under ”Tools”.
This only works if there is only one histogram per canvas. Otherwise, the program does not know which 
histogram to fit and crashes. Additionally, the x-axis must not be scaled. This causes bad fits. The fit panel 
has different predefined functions that can be found when opening the ”Func” section, like gaussians or Landau, etc. 
Any defined function of the ROOT class TF1 can also be found there. 
Therefore it is very well possible to use any function to do a fit. Under General the
Method, Fit and Draw Options can be chosen as well as the fitting range in the lower bar. Using ”Fit”,
”Draw” and ”Close” from the options in the bottom right, the fit can be drawn onto the histogram
and the Fit Panel closed. This Fit Panel is a good option to fit predefined functions onto individual
histograms if there are strongly varying start parameters necessary and if there is no way to do a fit
with a roughly defined function before fitting the actually necessary one like for the Gauss and Gumbel
fits.

## Installation
1. Git clone LEMING Browser from Gitlab to local PC.
2. Activate an environment in which ROOT is installed.
3. Go to leming-browser directory.


## Usage
1. Type the following into the terminal: python -i gui.py
2. In the application,  enter the paths to the ROOT files or use the "Browse Folders" button to select the folder using a file browser. A second, optimal dataset can be given.
3. Select the histograms of interest and choose if the default config file or a user-specific config file should be used. 
4. Click "Draw Selected histograms" to draw the histograms. They will open in the webbrowser. 
5. One can change the x axis range using SetRangeUser. If one wants to save the changes made to the x ranges if the histograms, click "Save Config" before closing the application.
6. To close the program, click the Exit button.

## Support
From October 2024 onwards, Rebecca Gartner is the acting responsible. Also Damian Goeldi. 