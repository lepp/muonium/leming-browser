import ROOT
import math
import gc
import numpy as np
import re

ROOT.gStyle.SetOptStat("e")
ROOT.TH1.SetDefaultSumw2()

# important functions:

colours = [
    ROOT.kBlack,
    ROOT.kRed,
    ROOT.kBlue,
    ROOT.kGreen,
    ROOT.kYellow,
    ROOT.kOrange,
    ROOT.kPink,
    ROOT.kViolet,
    ROOT.kAzure,
    ROOT.kTeal,
]
detector_suffix_L = ["L_F1aN1", "L_F2aN2", "L_F3aN3", "L_F4aN4", "L_F5aN5"]
detector_suffix_R = ["R_F1aN1", "R_F2aN2", "R_F3aN3", "R_F4aN4", "R_F5aN5"]

ecut_l = np.array(
    [
        [
            [50, 90],
            [80, 120],
            [60, 80],
            [170, 120],
            [200, 45],
        ],  # used for 1st cooldown data
        [
            [60, 130],
            [150, 200],
            [60, 190],
            [180, 220],
            [100, 100],
        ],  # used for 2nd cooldown
        [
            [60, 130],
            [185, 200],
            [60, 110],
            [200, 250],
            [140, 100],
        ],  # used for 3rd cooldown data
        [
            [60, 130],
            [185, 200],
            [100, 100],
            [150, 220],
            [60, 100],
        ],  # used for 4th cooldown data
    ]
)  # [cut on F, cut on N]
ecut_r = np.array(
    [
        [
            [130, 100],
            [130, 100],
            [90, 80],
            [80, 130],
            [50, 170],
        ],  # used for 1st cooldown data
        [
            [220, 200],
            [200, 130],
            [110, 150],
            [110, 200],
            [110, 190],
        ],  # used for 2nd cooldown
        [
            [220, 200],
            [200, 130],
            [110, 150],
            [130, 240],
            [110, 300],
        ],  # used for 3rd cooldown data
        [
            [220, 200],
            [200, 100],
            [90, 150],
            [110, 180],
            [90, 300],
        ],  # used for 4th cooldown data
    ]
)  # [cut on F, cut on N]

detector_suffix_cross_L = [
    "L_F1aN2",
    "L_F2aN1",
    "L_F2aN3",
    "L_F3aN2",
    "L_F4aN3",
    "L_F3aN4",
    "L_F4aN5",
]
detector_suffix_cross_R = [
    "R_F1aN2",
    "R_F2aN1",
    "R_F2aN3",
    "R_F3aN2",
    "R_F4aN3",
    "R_F3aN4",
    "R_F4aN5",
]
coincidences_cross = [
    "F1-N2",
    "F2-N1",
    "F2-N3",
    "F3-N2",
    "F4-N3",
    "F3-N4",
    "F4-N5",
]


def clearing_objects(file, indv_name):
    for idx in range(len(file)):
        file[idx].clear_objs(indv_name)


def energy_time_spectra_ecut(
    file,
    canv_name,
    canv_title,
    hist_name,
    indv_name,
    spectrum,
    det_names,
    det_number,
    range_start,
    range_end,
    ecut,
):
    canvas = file[0].create_canvas(canv_name, canv_title)

    canvas.Divide(4, 3)
    line_list = []

    for j in range(len(det_number)):
        line_list_local = []

        stack = file[0].create_stack(canv_name, canv_title)

        legend = file[0].create_legend(canv_name, canv_title)

        canvas.cd(j + 1)
        for idx in range(len(file)):
            projection = file[idx].get_projx(
                hist_name, indv_name, det_number[j], det_number[j]
            )

            legend.AddEntry(
                projection,
                f"Runs: {file[idx].get_name()}, #entries: {projection.GetEntries()}",
                "lep",
            )
            projection.SetTitle(f"{spectrum} of {det_names[j]}")
            projection.SetLineColor(colours[idx])

            # projection.GetXaxis().SetRangeUser(range_start, range_end)

            stack.Add(projection)
            # print("Ecut pos", ecut)
            # print("Ecut ", ecut_l[ecut[idx]][j][0])
            ecut_flat_l = ecut_l[ecut[idx]].flatten()
            ecut_flat_r = ecut_r[ecut[idx]].flatten()

            if re.search(r"left", canv_name) and ecut != [-2, -2]:
                Tline_ecut = file[0].create_stupid_tline(
                    f"ecut {det_names[j]}",
                    f"ecut {det_names[j]}",
                    ecut_flat_l[j],
                    colours[idx],
                )
                line_list.append(Tline_ecut)
                line_list_local.append(Tline_ecut)

                Tline_ecut.Draw()

            elif re.search(r"right", canv_name) and ecut != [-2, -2]:
                Tline_ecut = file[0].create_stupid_tline(
                    f"ecut {det_names[j]}",
                    f"ecut {det_names[j]}",
                    ecut_flat_r[j],
                    colours[idx],
                )
                line_list.append(Tline_ecut)
                line_list_local.append(Tline_ecut)
                Tline_ecut.Draw()

        stack.SetTitle(f"{spectrum} of {det_names[j]}")

        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")
        for line in line_list_local:
            line.Draw("SAME")

        legend.Draw()

        ROOT.gPad.Modified()
        ROOT.gPad.Update()
        canvas.Update()


def energy_time_spectra_no_ecut(
    file,
    canv_name,
    canv_title,
    hist_name,
    indv_name,
    spectrum,
    det_names,
    det_number,
    range_start,
    range_end,
):
    canvas = file[0].create_canvas(canv_name, canv_title)

    canvas.Divide(4, 3)
    line_list = []

    for j in range(len(det_number)):
        line_list_local = []

        stack = file[0].create_stack(canv_name, canv_title)

        legend = file[0].create_legend(canv_name, canv_title)

        canvas.cd(j + 1)
        for idx in range(len(file)):
            projection = file[idx].get_projx(
                hist_name, indv_name, det_number[j], det_number[j]
            )

            legend.AddEntry(
                projection,
                f"Runs: {file[idx].get_name()}, #entries: {projection.GetEntries()}",
                "lep",
            )
            projection.SetTitle(f"{spectrum} of {det_names[j]}")
            projection.SetLineColor(colours[idx])

            # projection.GetXaxis().SetRangeUser(range_start, range_end)

            stack.Add(projection)

        stack.SetTitle(f"{spectrum} of {det_names[j]}")

        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()

        ROOT.gPad.Modified()
        ROOT.gPad.Update()
        canvas.Update()


def lifetime_correction(histogram):
    bin_first = histogram.GetXaxis().FindBin(0.0)
    bin_last = histogram.GetXaxis().GetNbins()
    for bin in range(bin_first, bin_last):
        factor = math.exp(histogram.GetBinCenter(bin) / 2193.0)
        old_bin_cont = histogram.GetBinContent(bin)
        new_bin_cont = old_bin_cont * factor
        histogram.SetBinContent(bin, new_bin_cont)
        histogram.SetBinError(bin, histogram.GetBinError(bin) * factor)
    return histogram


def Normalize(histogram):
    integral = histogram.Integral(histogram.FindBin(0), histogram.FindBin(8000))
    histogram.Scale(1 / integral)
    return histogram


def degrassing(histogram):
    bin_first = histogram.GetXaxis().FindBin(0.0)
    bin_last = histogram.GetXaxis().GetNbins()
    # fit_straight1 = ROOT.TF1(
    #    f"fit_straight", "pol0", -1700, -200
    # )
    # histogram.Fit(fit_straight1, "N WL", "", -1700, -200)
    # par1 = fit_straight1.GetParameter(0)
    # fit exponential decay
    fit_expo = ROOT.TF1(f"fit_Expo", "[0]*exp(-(x+10000)/2193) +[1]", -10000, -200)
    param0 = 0
    bin_10000 = histogram.GetXaxis().FindBin(-9999)
    bin_8000 = histogram.GetXaxis().FindBin(-9500)
    bin_2000 = histogram.GetXaxis().FindBin(-1700)
    bin_500 = histogram.GetXaxis().FindBin(-200)
    bin_contents = []
    bin_contents2 = []
    for i in range(bin_10000, bin_8000):
        bin_value = histogram.GetBinContent(i)
        bin_contents.append(bin_value)
    for i in range(bin_2000, bin_500):
        bin_value = histogram.GetBinContent(i)
        bin_contents2.append(bin_value)
    bin_mean = sum(bin_contents) / len(bin_contents)
    offset_mean = sum(bin_contents2) / len(bin_contents2)
    print("init mean", bin_mean)
    fit_expo.SetParameters(bin_mean, offset_mean)
    # fit_expo.SetParameters(1, 0)
    fit_expo.SetParLimits(0, 0.3 * bin_mean, 10 * bin_mean)
    fit_expo.SetParLimits(1, 0, 3 * offset_mean)
    # fit_expo.FixParameter(1,offset_mean)

    histogram.Fit(fit_expo, "WL", "", -9000, -500)
    par1 = fit_expo.GetParameter(0)
    par2 = fit_expo.GetParameter(1)

    print("par1: ", par1, "par2: ", par2)

    helper_hist = histogram.Clone("helper")  # clone hist
    helper_hist.Reset()  # empty it
    for bin in range(bin_first, bin_last):
        #    print(histogram.GetBinCenter(bin))
        value = (
            par1 * math.exp(-((helper_hist.GetBinCenter(bin) + 10000) / 2193.0)) + par2
        )
        helper_hist.SetBinContent(
            bin, value
        )  # fill helper hist with values to subtract from original one

    histogram.Add(
        helper_hist, -1
    )  # subtract helper hist - does the degrassing without having to manually set the errors

    return histogram


# background subtraction: take bin data from bins 0 to 1000 and average these to get average background.
# Then subtract from bin content of rest.
def background_subtraction(histogram):
    bkg = 0.0
    bin_first = histogram.GetXaxis().FindBin(0.0)
    bin_last = histogram.GetXaxis().GetNbins()
    bkg_start = histogram.GetXaxis().FindBin(0.0)
    bkg_end = histogram.GetXaxis().FindBin(1000.0)
    for bin in range(bkg_start, bkg_end):
        bkg = bkg + histogram.GetBinContent(bin)

    bkg = bkg / (bkg_end - bkg_start)
    for bin in range(bin_first, bin_last):
        bin_cont = histogram.GetBinContent(bin)
        histogram.SetBinContent(bin, bin_cont - bkg)
    return histogram


def scale_n_draw(file, histogram, det_name, rebin_factor, col, stack, legend, scaling):
    norm_fct = file.get_object("hMuonEnergy").GetEntries()

    if scaling == True:
        histogram.Scale(1 / norm_fct)
    histogram.Rebin(rebin_factor)
    histogram.SetLineColor(col)

    normalized_number = (histogram.GetEntries() / norm_fct) * 1e6

    stack.Add(histogram)
    legend.AddEntry(
        histogram,
        f"Runs: {file.get_name()}, #entries: {histogram.GetEntries()}, normalized:{format(normalized_number, '.2f')} +- {format((np.sqrt(histogram.GetEntries())/norm_fct) * 1e6,'.2f' )}",
        "lep",
    )


# Muon Energy
def muon_energy(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("muon_energy")

    root_file[0].clear_stack("muon_energy")

    clearing_objects(root_file, "MuonEnergy")

    canvas = root_file[0].create_canvas("muon_energy", "Muon energy")

    hs = root_file[0].create_stack("muon_energy", "Muon energy")
    legend = root_file[0].create_legend("muon_energy", "Muon energy")

    for idx in range(len(root_file)):
        norm_fct = root_file[idx].get_clone("hMuonEnergy", "MuonEnergy").GetEntries()
        projection = root_file[idx].get_projx("hMuonEnergy", "MuonEnergy", 1, 1)
        projection.SetBinErrorOption(ROOT.TH1.kPoisson)
        print(
            projection,
            projection.GetBinContent(1500),
            projection.GetBinErrorUp(1500),
            projection.GetBinErrorLow(1500),
        )

        legend.AddEntry(
            projection,
            f"Run: {root_file[idx].get_name()}, #entries: {norm_fct}",
            "lep",
        )
        print(
            "muon energy underflow",
            projection.GetBinContent(0),
            "muon energy overflow",
            projection.GetBinContent(projection.GetNbinsX() + 1),
        )

        # projection.Scale(1 / norm_fct)
        projection.SetLineColor(colours[idx])

        hs.Add(projection)
    # hs.Draw("nostack E")
    projection.Draw("E same")
    # projection.GetXaxis().SetRangeUser(6600, 6610)

    # hs.Draw("nostack hist SAME")

    legend.Draw()
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    canvas.Update()
    # canvas.SaveAs("test.png")


# atomic electron Energy
def atomic_energy(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("atomic_energy")

    root_file[0].clear_stack("atomic_energy")

    clearing_objects(root_file, "atomicEnergy")

    canvas = root_file[0].create_canvas("atomic_energy", "atomic electron energy")

    hs = root_file[0].create_stack("atomic_energy", "atomic electron energy")
    legend = root_file[0].create_legend("atomic_energy", "atomic electron energy")

    for idx in range(len(root_file)):
        norm_fct = (
            root_file[idx].get_clone("hAtomicEnergy", "atomicEnergy").GetEntries()
        )
        projection = root_file[idx].get_projx("hAtomicEnergy", "atomicEnergy", 1, 1)
        legend.AddEntry(
            projection,
            f"Run: {root_file[idx].get_name()}, #entries: {norm_fct}",
            "lep",
        )

        projection.Scale(1 / norm_fct)
        projection.SetLineColor(colours[idx])

        hs.Add(projection)
    hs.Draw("nostack hist")
    legend.Draw()
    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    canvas.Update()


# Electron energy left
def electronenergy_left(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("electron_energy_left")
    root_file[0].clear_stack("electron_energy_left")

    histogram_name = "hElectronEnergy"
    individual_name = "ElectronEnergyLeft"

    clearing_objects(root_file, individual_name)
    detect_number = [1, 21, 5, 6, 9, 10, 13, 14, 17, 18]
    detect_names = [
        "Electron_LF1",
        "Electron_LN1",
        "Electron_LF2",
        "Electron_LN2",
        "Electron_LF3",
        "Electron_LN3",
        "Electron_LF4",
        "Electron_LN4",
        "Electron_LF5",
        "Electron_LN5",
    ]
    canvas_name = "electron_energy_left"
    canvas_title = "Electron Energy Left"
    spectrum_type = "Energy"
    energy_time_spectra_ecut(
        root_file,
        canvas_name,
        canvas_title,
        histogram_name,
        individual_name,
        spectrum_type,
        detect_names,
        detect_number,
        0,
        2000,
        ecut,
    )


# Electron energy right
def electronenergy_right(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("electron_energy_right")
    root_file[0].clear_stack("electron_energy_right")

    histogram_name = "hElectronEnergy"
    individual_name = "ElectronEnergyRight"
    clearing_objects(root_file, individual_name)
    detect_number = [2, 3, 6, 7, 10, 11, 15, 16, 18, 19]
    detect_names = [
        "Electron_RF1",
        "Electron_RN1",
        "Electron_RF2",
        "Electron_RN2",
        "Electron_RF3",
        "Electron_RN3",
        "Electron_RF4",
        "Electron_RN4",
        "Electron_RF5",
        "Electron_RN5",
    ]
    canvas_name = "electron_energy_right"
    canvas_title = "Electron Energy Right"
    spectrum_type = "Energy"
    energy_time_spectra_ecut(
        root_file,
        canvas_name,
        canvas_title,
        histogram_name,
        individual_name,
        spectrum_type,
        detect_names,
        detect_number,
        0,
        2000,
        ecut,
    )


# Electron time left
def electrontime_left(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("electron_time_left")
    root_file[0].clear_stack("electron_time_left")

    histogram_name = "hElectronVsTime"
    individual_name = "ElectronTimeLeft"
    clearing_objects(root_file, individual_name)
    detect_number = [1, 21, 5, 6, 9, 10, 13, 14, 17, 18]

    detect_names = [
        "Electron_LF1",
        "Electron_LN1",
        "Electron_LF2",
        "Electron_LN2",
        "Electron_LF3",
        "Electron_LN3",
        "Electron_LF4",
        "Electron_LN4",
        "Electron_LF5",
        "Electron_LN5",
    ]
    canvas_name = "electron_time_left"
    canvas_title = "Electron Time Left"
    spectrum_type = "Time"
    energy_time_spectra_no_ecut(
        root_file,
        canvas_name,
        canvas_title,
        histogram_name,
        individual_name,
        spectrum_type,
        detect_names,
        detect_number,
        -1000,
        1000,
    )


# Electron time right
def electrontime_right(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("electron_time_right")
    root_file[0].clear_stack("electron_time_right")

    histogram_name = "hElectronVsTime"
    individual_name = "ElectronTimeRight"
    clearing_objects(root_file, individual_name)
    detect_number = [2, 3, 6, 7, 10, 11, 15, 16, 18, 19]

    detect_names = [
        "Electron_RF1",
        "Electron_RN1",
        "Electron_RF2",
        "Electron_RN2",
        "Electron_RF3",
        "Electron_RN3",
        "Electron_RF4",
        "Electron_RN4",
        "Electron_RF5",
        "Electron_RN5",
    ]
    canvas_name = "electron_time_right"
    canvas_title = "Electron Time Right"
    spectrum_type = "Time"
    energy_time_spectra_no_ecut(
        root_file,
        canvas_name,
        canvas_title,
        histogram_name,
        individual_name,
        spectrum_type,
        detect_names,
        detect_number,
        -1000,
        1000,
    )


coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]


def coin_LR_base(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv(
        f"coin_lr_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )
    root_file[0].clear_stack(
        f"coin_lr_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )

    rebin_fct = 1  # never set me above 1 there is another rebin factor later *which acts before the degrass*
    coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    canvas = root_file[0].create_canvas(
        f"coin_lr_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
        "Coincidences of Left and Right added with lifetime correction and degrassed and E cut",
    )
    canvas.Divide(3, 2)

    histogram_dict = dict()

    for j in range(0, 5):

        # get the wanted histograms

        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)
        canvas.cd(j + 1)
        hist_list = []

        for idx in range(len(root_file)):

            if ecut != [-2, -2]:
                hist_coin_L = root_file[idx].get_hist_from_tree(
                    individual_name_L, individual_name_L, ecut_l[ecut[idx]][j]
                )
                hist_coin_R = root_file[idx].get_hist_from_tree(
                    individual_name_R, individual_name_R, ecut_r[ecut[idx]][j]
                )
            elif ecut == [-2, -2]:
                hist_coin_R = root_file[idx].get_clone(
                    detector_name_R, individual_name_R
                )
                hist_coin_L = root_file[idx].get_clone(
                    detector_name_L, individual_name_L
                )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_L.Sumw2()
            hist_coin_R.Sumw2()

            # normalise
            hist_coin_L = Normalize(hist_coin_L)
            hist_coin_R = Normalize(hist_coin_R)

            hist_coin_L.Add(hist_coin_R)
            hist_coin_L.Rebin(2)

            hist_list.append(hist_coin_L)

        histogram_dict[f"CoincidencePP_{detector_suffix_L[j]}"] = hist_list
    return histogram_dict, canvas


def coin_LR_options(root_file, tcorr, degras, ecut, bkg_sub):
    histogram_dict, canvas = coin_LR_base(root_file, tcorr, degras, ecut, bkg_sub)

    for j in range(0, 5):

        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        canvas.cd(j + 1)

        conditions = []

        if tcorr:
            conditions.append("lifetime corrected")
        if bkg_sub:
            conditions.append("bkg subtracted")
        if degras:
            conditions.append("degrased")
        if ecut != [-2, -2]:
            conditions.append("E cut")

        title = ", ".join(conditions) if conditions else ""
        stack = root_file[0].create_stack(
            f"coin_lr_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences of Left and Right added, {title}",
        )

        legend = root_file[0].create_legend(
            f"coin_lr_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences of Left and Right added, {title}",
        )
        for idx in range(len(root_file)):
            hist = histogram_dict[f"CoincidencePP_{detector_suffix_L[j]}"][idx]
            if degras:
                hist = degrassing(hist)
            if tcorr:
                hist = lifetime_correction(hist)
            if bkg_sub:
                hist = background_subtraction(hist)

            hist.SetTitle(f"Coincidences L&R added, {coincidences[j]}, {title}")
            scale_n_draw(
                root_file[idx],
                hist,
                detector_name_L,
                1,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
        stack.SetTitle(f"Coincidences L&R added, {coincidences[j]}, {title}")
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


def coin_L_base(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv(
        f"coin_l_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )
    root_file[0].clear_stack(
        f"coin_l_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )

    rebin_fct = 1  # never set me above 1 there is another rebin factor later *which acts before the degrass*
    canvas = root_file[0].create_canvas(
        f"coin_l_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
        "Coincidences Left with lifetime correction and degrassed and E cut",
    )
    canvas.Divide(3, 2)

    histogram_dict = dict()

    for j in range(0, 5):
        # get the wanted histograms

        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"

        clearing_objects(root_file, individual_name_L)
        canvas.cd(j + 1)
        hist_list = []

        for idx in range(len(root_file)):

            if ecut != [-2, -2]:
                hist_coin_L = root_file[idx].get_hist_from_tree(
                    individual_name_L, individual_name_L, ecut_l[ecut[idx]][j]
                )

            elif ecut == [-2, -2]:

                hist_coin_L = root_file[idx].get_clone(
                    detector_name_L, individual_name_L
                )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_L.Sumw2()

            # normalise
            hist_coin_L = Normalize(hist_coin_L)

            hist_coin_L.Rebin(2)

            hist_list.append(hist_coin_L)

        histogram_dict[f"CoincidencePP_{detector_suffix_L[j]}"] = hist_list
    return histogram_dict, canvas


def coin_L_options(root_file, tcorr, degras, ecut, bkg_sub):
    histogram_dict, canvas = coin_L_base(root_file, tcorr, degras, ecut, bkg_sub)

    for j in range(0, 5):
        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"

        canvas.cd(j + 1)

        conditions = []

        if tcorr:
            conditions.append("lifetime corrected")
        if bkg_sub:
            conditions.append("bkg subtracted")
        if degras:
            conditions.append("degrased")
        if ecut != [-2, -2]:
            conditions.append("E cut")

        title = ", ".join(conditions) if conditions else ""
        stack = root_file[0].create_stack(
            f"coin_l_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Left, {title}",
        )

        legend = root_file[0].create_legend(
            f"coin_l_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Left, {title}",
        )
        for idx in range(len(root_file)):
            hist = histogram_dict[f"CoincidencePP_{detector_suffix_L[j]}"][idx]
            if degras:
                hist = degrassing(hist)
            if tcorr:
                hist = lifetime_correction(hist)
            if bkg_sub:
                hist = background_subtraction(hist)

            hist.SetTitle(f"Coincidences L, {coincidences[j]}, {title}")
            scale_n_draw(
                root_file[idx],
                hist,
                detector_name_L,
                1,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
        stack.SetTitle(f"Coincidences L, {coincidences[j]}, {title}")
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


def coin_R_base(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv(
        f"coin_r_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )
    root_file[0].clear_stack(
        f"coin_r_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )

    rebin_fct = 1  # never set me above 1 there is another rebin factor later *which acts before the degrass*
    canvas = root_file[0].create_canvas(
        f"coin_r_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
        "Coincidences Right  with lifetime correction and degrassed and E cut",
    )
    canvas.Divide(3, 2)

    histogram_dict = dict()

    for j in range(0, 5):
        # get the wanted histograms

        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_R)
        canvas.cd(j + 1)
        hist_list = []

        for idx in range(len(root_file)):

            if ecut != [-2, -2]:

                hist_coin_R = root_file[idx].get_hist_from_tree(
                    individual_name_R, individual_name_R, ecut_r[ecut[idx]][j]
                )
            elif ecut == [-2, -2]:
                hist_coin_R = root_file[idx].get_clone(
                    detector_name_R, individual_name_R
                )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_R.Sumw2()

            # normalise
            hist_coin_R = Normalize(hist_coin_R)

            hist_coin_R.Rebin(2)

            hist_list.append(hist_coin_R)

        histogram_dict[f"CoincidencePP_{detector_suffix_R[j]}"] = hist_list
    return histogram_dict, canvas


def coin_R_options(root_file, tcorr, degras, ecut, bkg_sub):
    histogram_dict, canvas = coin_R_base(root_file, tcorr, degras, ecut, bkg_sub)

    for j in range(0, 5):

        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        canvas.cd(j + 1)

        conditions = []

        if tcorr:
            conditions.append("lifetime corrected")
        if bkg_sub:
            conditions.append("bkg subtracted")
        if degras:
            conditions.append("degrased")
        if ecut != [-2, -2]:
            conditions.append("E cut")

        title = ", ".join(conditions) if conditions else ""
        stack = root_file[0].create_stack(
            f"coin_r_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Right, {title}",
        )

        legend = root_file[0].create_legend(
            f"coin_r_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Right, {title}",
        )
        for idx in range(len(root_file)):
            hist = histogram_dict[f"CoincidencePP_{detector_suffix_R[j]}"][idx]
            if degras:
                hist = degrassing(hist)
            if tcorr:
                hist = lifetime_correction(hist)
            if bkg_sub:
                hist = background_subtraction(hist)

            hist.SetTitle(f"Coincidences R, {coincidences[j]}, {title}")
            scale_n_draw(
                root_file[idx],
                hist,
                detector_name_R,
                1,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
        stack.SetTitle(f"Coincidences R, {coincidences[j]}, {title}")
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


def coin_cross_base(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv(
        f"coin_cross_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )
    root_file[0].clear_stack(
        f"coin_cross_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}"
    )

    rebin_fct = 1  # never set me above 1 there is another rebin factor later *which acts before the degrass*
    coincidences = coincidences_cross
    canvas = root_file[0].create_canvas(
        f"coin_cross_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
        "Coincidences cross added with lifetime correction and degrassed and E cut",
    )
    canvas.Divide(3, 3)

    histogram_dict = dict()

    for j in range(0, len(detector_suffix_cross_L)):
        # get the wanted histograms

        detector_name_L = f"hCoincidencePP_{detector_suffix_cross_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_cross_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_cross_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_cross_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)
        canvas.cd(j + 1)
        hist_list = []

        for idx in range(len(root_file)):
            if ecut != [-2, -2]:
                hist_coin_L = root_file[idx].get_hist_from_tree(
                    individual_name_L,
                    individual_name_L,
                    np.array([0, 0]),  # dummy, imprve me
                )
                hist_coin_R = root_file[idx].get_hist_from_tree(
                    individual_name_R, individual_name_R, np.array([0, 0])
                )

            elif ecut == [-2, -2]:
                hist_coin_R = root_file[idx].get_clone(
                    detector_name_R, individual_name_R
                )
                hist_coin_L = root_file[idx].get_clone(
                    detector_name_L, individual_name_L
                )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_L.Sumw2()
            hist_coin_R.Sumw2()

            # normalise
            hist_coin_L = Normalize(hist_coin_L)
            hist_coin_R = Normalize(hist_coin_R)

            hist_coin_L.Add(hist_coin_R)
            hist_coin_L.Rebin(2)

            hist_list.append(hist_coin_L)

        histogram_dict[f"CoincidencePP_{detector_suffix_cross_L[j]}"] = hist_list
    return histogram_dict, canvas


def coin_cross_options(root_file, tcorr, degras, ecut, bkg_sub):
    histogram_dict, canvas = coin_cross_base(root_file, tcorr, degras, ecut, bkg_sub)

    for j in range(0, len(detector_suffix_cross_L)):
        detector_name_L = f"hCoincidencePP_{detector_suffix_cross_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_cross_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_cross_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_cross_R[j]}"
        canvas.cd(j + 1)

        conditions = []

        if tcorr:
            conditions.append("lifetime corrected")
        if bkg_sub:
            conditions.append("bkg subtracted")
        if degras:
            conditions.append("degrased")
        if ecut != [-2, -2]:
            conditions.append("E cut")

        title = ", ".join(conditions) if conditions else ""
        stack = root_file[0].create_stack(
            f"coin_cross_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Cross (L&R), {title}",
        )

        legend = root_file[0].create_legend(
            f"coin_cross_ecut_{ecut}_degras_{degras}_tcorr_{tcorr}_bkg_sub_{bkg_sub}",
            f"Coincidences Cross (L&R), {title}",
        )
        for idx in range(len(root_file)):
            hist = histogram_dict[f"CoincidencePP_{detector_suffix_cross_L[j]}"][idx]
            if degras:
                hist = degrassing(hist)
            if tcorr:
                hist = lifetime_correction(hist)
            if bkg_sub:
                hist = background_subtraction(hist)

            hist.SetTitle(
                f"Coincidences Cross L&R added, {coincidences_cross[j]}, {title}"
            )
            scale_n_draw(
                root_file[idx],
                hist,
                detector_name_L,
                1,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
        stack.SetTitle(
            f"Coincidences Cross L&R added, {coincidences_cross[j]}, {title}"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


####Pauls bit
def coincidence_LR_assy(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("coin_lr_assy")
    root_file[0].clear_stack("coin_lr_assy")

    rebin_fct = 1
    coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    canvas = root_file[0].create_canvas("coin_lr_assy", "Coincidence LR Assy")
    canvas.Divide(3, 2)

    for j in range(len(detector_suffix_L)):
        # get the wanted histograms
        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_L = f"Coin_L_Assy_{detector_suffix_L[j]}"
        individual_name_R = f"Coin_R_Assy_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)

        canvas.cd(j + 1)
        stack = root_file[0].create_stack(f"coin_lr_assy", "Coincidence LR Assy")

        legend = root_file[0].create_legend(f"coin_lr_assy", "Coincidence LR Assy")

        for idx in range(len(root_file)):
            histo_R = root_file[idx].get_clone(detector_name_R, individual_name_R)
            histo = root_file[idx].get_clone(detector_name_L, individual_name_L)
            histtmp = histo.Clone("gerald")

            # store errors
            histo.Sumw2()
            histo_R.Sumw2()
            histtmp.Sumw2()

            # normalise
            # histo = Normalize(histo)
            # histo_R = Normalize(histo_R)
            # histtmp = Normalize(histtmp)

            intL = histo.Integral(histo.FindBin(0), histo.FindBin(8000))
            intR = histo_R.Integral(histo.FindBin(0), histo.FindBin(8000))
            print("L/R", intL, intR)
            # scalefactor = intL/intR
            # scale right histo to left histo
            # histo_R.Sc ale(scalefactor)
            histo.Add(histo_R, -1)  # L-R
            histtmp.Add(histo_R)  # L+R
            histbins = histtmp.GetNbinsX()
            histtmpbins = histtmp.GetNbinsX()
            print("histbins", histbins, histtmpbins)

            # rebin the histos before the divide
            histo.Rebin(8)
            histtmp.Rebin(8)
            # histo_pois = ROOT.TGraphAsymmErrors()
            # histo_pois.Divide(histo, histtmp, "pois midp")
            histo.Divide(histtmp)

            histo.SetTitle(
                f"Electron coincidence of left and right {coincidences[j]} assy"
            )
            scale_n_draw(
                root_file[idx],
                histo,
                #                histtmp,
                individual_name_L,
                rebin_fct,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
            # fit exponential decay
            fit_exp_coin_lr = ROOT.TF1(
                f"fit_coin_lr_{coincidences[j]}", "[0]*TMath::Exp(-x/2193)", 0, 10000
            )
            fit_exp_coin_lr.SetParameters(10)
            fit_exp_coin_lr.SetLineColor(colours[idx])
            histo.Fit(fit_exp_coin_lr, "QN WL", "", 0, 10000)
        stack.SetTitle(
            f"Electron coincidence of left and right {coincidences[j]}, L-R/L+R"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


# Coincidences of the left and right detectors with lifetime corr, background subtraction, added, degrassed, ecut
def coincidence_LR_added_degrassed_ecut_with_eff_fit(
    root_file, tcorr, degras, ecut, bkg_sub
):
    root_file[0].clear_canv("coin_lr_tcorr_degrassed_ecut_fit")
    root_file[0].clear_stack("coin_lr_tcorr_degrassed_ecut_fit")
    fit_list = []

    rebin_fct = 1  # never set me above 1 there is another rebin factor later *which acts before the degrass*
    coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    canvas = root_file[0].create_canvas(
        "coin_lr_tcorr_degrassed_ecut_fit",
        "Coincidences of Left and Right added with lifetime correction, degrassed, E cut, fitted",
    )
    canvas.Divide(3, 2)

    for j in range(0, 5):
        # get the wanted histograms

        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)

        canvas.cd(j + 1)
        stack = root_file[0].create_stack(
            f"coin_lr_tcorr_degrassed_ecut_fit",
            "Coincidences of Left and Right added with lifetime correction and degrassed Ecut, fitted",
        )

        legend = root_file[0].create_legend(
            f"coin_lr_tcorr_degrassed_ecut_fit",
            "Coincidences of Left and Right added with lifetime correction and degrassed Ecut, fitted",
        )
        fit_list_local = []

        for idx in range(len(root_file)):

            hist_coin_L = root_file[idx].get_hist_from_tree(
                individual_name_L, individual_name_L, ecut_l[ecut[idx]][j]
            )
            hist_coin_R = root_file[idx].get_hist_from_tree(
                individual_name_R, individual_name_R, ecut_r[ecut[idx]][j]
            )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_L.Sumw2()
            hist_coin_R.Sumw2()

            # normalise
            hist_coin_L = Normalize(hist_coin_L)
            hist_coin_R = Normalize(hist_coin_R)

            hist_coin_L.Add(hist_coin_R)
            # perform degrassing
            hist_coin_L.Rebin(2)
            hist_coin_L = degrassing(hist_coin_L)

            # perform lifetime correction on left coincidence histogram:
            hist_coin_L = lifetime_correction(hist_coin_L)

            # perform background correction on left coincidence histogram:
            # hist_coin_L = background_subtraction(hist_coin_L)

            if (j == 0) or (j == 1):

                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, lifetime corr, {coincidences[j]} degrassed Ecut, fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
                # fit straight line
                fit_straight1 = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}", "pol0", 0, 500
                )
                fit_straight1.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1, "N", "", 0, 500)
                fit_list.append(fit_straight1)
                fit_list_local.append(fit_straight1)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                constant = fit_straight1.GetParameter(0)
                # fit straight line
                fit_straight2 = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}2_{idx}",
                    "pol0",
                    6000,
                    9000,
                )
                fit_straight2.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight2, "N", "", 6000, 9000)
                constant2 = fit_straight2.GetParameter(0)
                fit_list.append(fit_straight2)
                fit_list_local.append(fit_straight2)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                legend.AddEntry(
                    fit_straight2,
                    f"efficiency: {format(constant2/constant, '.2f')}",
                    "L",
                )
                print("eff:", constant2 / constant)
            elif j == 2:
                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, lifetime corr, {coincidences[j]} degrassed Ecut, fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
            elif j > 2:
                hist_coin_L = background_subtraction(hist_coin_L)
                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, t corr, {coincidences[j]} degrassed Ecut, BKG SUB, fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )

                # Gumbel fit:
                # Gauss part to get mu / center
                fit_gauss_coin_lr = ROOT.TF1(
                    f"fit_gauss_coin_lr_{coincidences[j]}_{idx}", "gaus", 0, 10000
                )
                hist_coin_L.Fit(fit_gauss_coin_lr, "QN", "", 0, 10000)
                gauss_parameter = fit_gauss_coin_lr.GetParameters()
                mu = gauss_parameter[1]

                fit_gumbel_coin_lr = ROOT.TF1(
                    f"fit_gumbel_coin_lr_{coincidences[j]}_{idx}",
                    "[2]/[0]*TMath::Exp(-((x-[1])/[0] + TMath::Exp(-(x-[1])/[0])))",
                    0,
                    10000,
                )
                fit_gumbel_coin_lr.SetParameters(100, mu, 1000)
                # fit_gumbel_coin_lr.SetParLimits(0, 500, 5000)
                fit_gumbel_coin_lr.SetParLimits(2, 500, 1e7)
                # fit_gumbel_coin_lr.SetParLimits(1, 0.1, 10000)
                fit_gumbel_coin_lr.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_gumbel_coin_lr, "", "", 0, 10000)
                fit_list_local.append(fit_gumbel_coin_lr)
                print(
                    f"Gumbel Mu for dataset {idx+1}, coincidence {coincidences[j]}: ",
                    fit_gumbel_coin_lr.GetParameter(1),
                )
                legend.AddEntry(
                    fit_gumbel_coin_lr,
                    f"mu: {format(fit_gumbel_coin_lr.GetParameter(1), '.2f')}",
                    "L",
                )
        stack.SetTitle(
            f"Coincidences L&R added, t corr, {coincidences[j]} degrassed Ecut, fitted, 4-5:BKG SUB"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")
        for fit in fit_list_local:
            fit.Draw("SAME")

        legend.Draw()
        canvas.Update()


def coincidence_gumbel_fit(root_file, tcorr, degras, ecut, bkg_sub):

    # fit only works on coincidences F2-N2, F3-N3, F4-N4
    rebin_fct = 1
    detector_suffix_gumbel_L = ["L_F2aN2", "L_F3aN3", "L_F4aN4", "L_F5aN5"]
    detector_suffix_gumbel_R = ["R_F2aN2", "R_F3aN3", "R_F4aN4", "R_F5aN5"]
    coincidences = ["F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    root_file[0].clear_canv("coin_gumbel_fit")
    root_file[0].clear_stack("coin_gumbel_fit")

    canvas = root_file[0].create_canvas(
        "coin_gumbel_fit", "Coincidences of Left and Right with Gumbel"
    )
    canvas.Divide(3, 2)

    for j in range(len(coincidences)):
        # get the wanted histograms
        detector_name_L = f"hCoincidencePP_{detector_suffix_gumbel_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_gumbel_R[j]}"
        individual_name_L = f"Coin_L_added_gumbel_{detector_suffix_L[j]}"
        individual_name_R = f"Coin_R_added_gumbel_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)

        canvas.cd(j + 1)
        stack = root_file[0].create_stack(
            f"coin_gumbel_fit", "Coincidences of Left and Right with Gumbel"
        )
        legend = root_file[0].create_legend(
            f"coin_gumbel_fit", "Coincidences of Left and Right with Gumbel"
        )
        for idx in range(len(root_file)):
            hist_coin_L = root_file[idx].get_clone(detector_name_L, individual_name_L)
            hist_coin_R = root_file[idx].get_clone(detector_name_R, individual_name_R)
            hist_coin_L.Sumw2()
            hist_coin_R.Sumw2()
            # perform lifetime correction on left coincidence histogram:
            hist_coin_L = lifetime_correction(hist_coin_L)

            # perform lifetime correction on right coincidence histogram:
            hist_coin_R = lifetime_correction(hist_coin_R)

            # perform background correction on left coincidence histogram:
            hist_coin_L = background_subtraction(hist_coin_L)

            # perform background correction on right coincidence histogram:
            hist_coin_R = background_subtraction(hist_coin_R)

            # add left and right histogram, rebin, set name and title:
            hist_coin_L.Add(hist_coin_R)
            hist_coin_L.SetTitle(
                f"Coincidences L&R added, lifetime corr, background sub, Gumbel fit {coincidences[j]}"
            )
            # For this function, if the scaling is set to True, the starting parameters may need to be adjusted...
            scale_n_draw(
                root_file[idx],
                hist_coin_L,
                individual_name_L,
                rebin_fct,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )

            # Gumbel fit:
            # Gauss part to get mu / center
            fit_gauss_coin_lr = ROOT.TF1(
                f"fit_gauss_coin_lr_{coincidences[j]}", "gaus", 0, 10000
            )
            hist_coin_L.Fit(fit_gauss_coin_lr, "QN WL", "", 0, 10000)
            gauss_parameter = fit_gauss_coin_lr.GetParameters()
            mu = gauss_parameter[1]

            fit_gumbel_coin_lr = ROOT.TF1(
                f"fit_gumbel_coin_lr_{coincidences[j]}",
                "[2]/[0]*TMath::Exp(-((x-[1])/[0] + TMath::Exp(-(x-[1])/[0])))",
                0,
                10000,
            )
            fit_gumbel_coin_lr.SetParameters(1, mu)
            fit_gumbel_coin_lr.SetParLimits(0, 500, 5000)
            fit_gumbel_coin_lr.SetParLimits(2, 1000, 1e7)
            fit_gumbel_coin_lr.SetParLimits(1, 0.1, 100000)
            fit_gumbel_coin_lr.SetLineColor(colours[idx])
            hist_coin_L.Fit(fit_gumbel_coin_lr, "WL", "", 0, 10000)
            print(
                f"Gumbel Mu for dataset {idx+1}, coincidence {coincidences[j]}: ",
                fit_gumbel_coin_lr.GetParameter(1),
            )
        stack.SetTitle(
            f"Coincidences L&R added, lifetime corr, background sub, Gumbel fit {coincidences[j]}"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


detector_suffix_L_triple_N = [
    "L_F1aqN1aN2p",
    "L_F2aqN2aqN1oN3pp",
    "L_F3aqN3aqN2oN4pp",
    "L_F4aqN4aqN3oN5pp",
    "L_F5aqN5aN4p",
]
detector_suffix_L_triple_F = [
    "L_F1aqN1aF2p",
    "L_F2aqN2aqF1oF3pp",
    "L_F3aqN3aqF2oF4pp",
    "L_F4aqN4aqF3oF5pp",
    "L_F5aqN5aF4p",
]
detector_suffix_R_triple_N = [
    "R_F1aqN1aN2p",
    "R_F2aqN2aqN1oN3pp",
    "R_F3aqN3aqN2oN4pp",
    "R_F4aqN4aqN3oN5pp",
    "R_F5aqN5aN4p",
]
detector_suffix_R_triple_F = [
    "R_F1aqN1aF2p",
    "R_F2aqN2aqF1oF3pp",
    "R_F3aqN3aqF2oF4pp",
    "R_F4aqN4aqF3oF5pp",
    "R_F5aqN5aF4p",
]


def coincidence_LR_added_triple_coin(root_file, tcorr, degras, ecut, bkg_sub):
    root_file[0].clear_canv("coin_lr_triple")
    root_file[0].clear_stack("coin_lr_triple")

    rebin_fct = 20
    coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    canvas = root_file[0].create_canvas("coin_lr_triple", "Triple Coincidence LR Added")
    canvas.Divide(3, 2)

    for j in range(len(detector_suffix_R_triple_F)):
        # get the wanted histograms
        detector_name_L_N = f"CoincidencePP_{detector_suffix_L_triple_N[j]}"
        detector_name_R_N = f"CoincidencePP_{detector_suffix_R_triple_N[j]}"
        individual_name_L_N = detector_name_L_N
        individual_name_R_N = detector_name_R_N
        detector_name_L_F = f"CoincidencePP_{detector_suffix_L_triple_F[j]}"
        detector_name_R_F = f"CoincidencePP_{detector_suffix_R_triple_F[j]}"
        individual_name_L_F = detector_name_L_F
        individual_name_R_F = detector_name_R_F
        clearing_objects(root_file, individual_name_L_F)
        clearing_objects(root_file, individual_name_R_F)
        clearing_objects(root_file, individual_name_L_N)
        clearing_objects(root_file, individual_name_R_N)

        canvas.cd(j + 1)
        stack = root_file[0].create_stack(
            f"coin_lr_triple", "Triple Coincidence LR Added"
        )

        legend = root_file[0].create_legend(
            f"coin_lr_triple", "Triple Coincidence LR Added"
        )

        for idx in range(len(root_file)):
            # histo_R = root_file[idx].get_clone(detector_name_R, individual_name_R)
            # histo = root_file[idx].get_clone(detector_name_L, individual_name_L)

            histo = root_file[idx].get_hist_from_tree_triple_coin_F(
                individual_name_L_F, individual_name_L_F, ecut_l[ecut[idx]][j]
            )
            histo_R = root_file[idx].get_hist_from_tree_triple_coin_F(
                individual_name_R_F, individual_name_R_F, ecut_r[ecut[idx]][j]
            )

            histo_N = root_file[idx].get_hist_from_tree_triple_coin_N(
                individual_name_L_N, individual_name_L_N, ecut_l[ecut[idx]][j]
            )
            histo_R_N = root_file[idx].get_hist_from_tree_triple_coin_N(
                individual_name_R_N, individual_name_R_N, ecut_r[ecut[idx]][j]
            )

            histo_R.Sumw2()
            histo.Sumw2()
            # Normalize(histo_R)
            # Normalize(histo)
            # add left and right histogram
            histo.Add(histo_R)

            histo_R_N.Sumw2()
            histo_N.Sumw2()
            # Normalize(histo_R_N)
            # Normalize(histo_N)
            # add left and right histogram
            histo_N.Add(histo_R_N)

            histo.Add(histo_N)

            histo.SetTitle(
                f"Electron triple coincidence of left and right {coincidences[j]}"
            )
            scale_n_draw(
                root_file[idx],
                histo,
                individual_name_L_N,
                rebin_fct,
                colours[idx],
                stack,
                legend,
                scaling=False,
            )
            # fit exponential decay
            fit_exp_coin_lr = ROOT.TF1(
                f"fit_coin_lr_{coincidences[j]}", "[0]*TMath::Exp(-x/2193)", 0, 10000
            )
            fit_exp_coin_lr.SetParameters(10)
            fit_exp_coin_lr.SetLineColor(colours[idx])
            histo.Fit(fit_exp_coin_lr, "QN WL", "", 0, 10000)
        stack.SetTitle(
            f"Electron energy triple coincidence of left and right {coincidences[j]}"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")

        legend.Draw()
        canvas.Update()


detector_suffix_cross = ["L_F1aN2", "L_F2aN1", "R_F1aN2", "R_F2aN1"]


# Coincidences of the left and right detectors with lifetime corr, background subtraction, added, degrassed, ecut
def coincidence_LR_added_degrassed_ecut_with_eff_fit_flat(
    root_file, tcorr, degras, ecut, bkg_sub
):
    root_file[0].clear_canv("coin_lr_tcorr_degrassed_ecut_fit_flat")
    root_file[0].clear_stack("coin_lr_tcorr_degrassed_ecut_fit_flat")
    fit_list = []

    rebin_fct = 1
    coincidences = ["F1-N1", "F2-N2", "F3-N3", "F4-N4", "F5-N5"]
    canvas = root_file[0].create_canvas(
        "coin_lr_tcorr_degrassed_ecut_fit_flat",
        "Coincidences of Left and Right added with lifetime correction, degrassed, E cut, flat fitted",
    )
    canvas.Divide(3, 2)

    for j in range(0, 5):
        # get the wanted histograms

        detector_name_L = f"hCoincidencePP_{detector_suffix_L[j]}"
        individual_name_L = f"CoincidencePP_{detector_suffix_L[j]}"
        detector_name_R = f"hCoincidencePP_{detector_suffix_R[j]}"
        individual_name_R = f"CoincidencePP_{detector_suffix_R[j]}"
        clearing_objects(root_file, individual_name_L)
        clearing_objects(root_file, individual_name_R)

        canvas.cd(j + 1)
        stack = root_file[0].create_stack(
            f"coin_lr_tcorr_degrassed_ecut_fit_flat",
            "Coincidences of Left and Right added with lifetime correction and degrassed Ecut, flat fitted",
        )

        legend = root_file[0].create_legend(
            f"coin_lr_tcorr_degrassed_ecut_fit_flat",
            "Coincidences of Left and Right added with lifetime correction and degrassed Ecut, flat fitted",
        )
        fit_list_local = []
        band_list_local = []

        for idx in range(len(root_file)):

            hist_coin_L = root_file[idx].get_hist_from_tree(
                individual_name_L, individual_name_L, ecut_l[ecut[idx]][j]
            )
            hist_coin_R = root_file[idx].get_hist_from_tree(
                individual_name_R, individual_name_R, ecut_r[ecut[idx]][j]
            )

            norm_fct = root_file[idx].get_object("hMuonEnergy").GetEntries()

            hist_coin_L.Sumw2()
            hist_coin_L.SetBinErrorOption(ROOT.TH1.kPoisson)
            hist_coin_R.Sumw2()
            hist_coin_R.SetBinErrorOption(ROOT.TH1.kPoisson)

            # normalise
            hist_coin_L = Normalize(hist_coin_L)
            hist_coin_R = Normalize(hist_coin_R)

            hist_coin_L.Add(hist_coin_R)
            # perform degrassing
            hist_coin_L.Rebin(2)
            hist_coin_L = degrassing(hist_coin_L)

            # perform lifetime correction on left coincidence histogram:
            hist_coin_L = lifetime_correction(hist_coin_L)

            # perform background correction on left coincidence histogram:
            # hist_coin_L = background_subtraction(hist_coin_L)

            if j == 0:

                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, lifetime corr, {coincidences[j]} degrassed Ecut, flat fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
                # fit straight line
                fit_straight1 = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}", "pol0", 0, 500
                )
                fit_straight1.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1, "N WL", "", 0, 500)
                fit_list.append(fit_straight1)
                fit_list_local.append(fit_straight1)
                constant = fit_straight1.GetParameter(0)

                band1 = ROOT.TBox(
                    0,
                    fit_straight1.GetParameter(0) - fit_straight1.GetParError(0),
                    500,
                    fit_straight1.GetParameter(0) + fit_straight1.GetParError(0),
                )
                band1.SetFillColorAlpha(colours[idx], 0.3)
                band1.SetLineColor(colours[idx])
                band_list_local.append(band1)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                # fit straight line
                fit_straight2 = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}2_{idx}",
                    "pol0",
                    6000,
                    9000,
                )
                fit_straight2.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight2, "N WL", "", 6000, 9000)
                constant2 = fit_straight2.GetParameter(0)
                fit_list.append(fit_straight2)
                fit_list_local.append(fit_straight2)

                # Create a TBox for the band
                band_ = ROOT.TBox(
                    6000,
                    fit_straight2.GetParameter(0) - fit_straight2.GetParError(0),
                    9000,
                    fit_straight2.GetParameter(0) + fit_straight2.GetParError(0),
                )
                band_.SetFillColorAlpha(colours[idx], 0.3)  # Semi-transparent red
                band_.SetLineColor(colours[idx])
                band_list_local.append(band_)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                legend.AddEntry(
                    fit_straight2,
                    f"efficiency: {format(constant2/constant, '.2f')}",
                    "L",
                )
                print("eff:", constant2 / constant)

            elif j == 1:

                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, lifetime corr, {coincidences[j]} degrassed Ecut, flat fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
                # fit straight line
                fit_straight1_a = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}", "pol0", 0, 500
                )
                fit_straight1_a.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_a, "N WL", "", 0, 500)
                fit_list.append(fit_straight1_a)
                fit_list_local.append(fit_straight1_a)
                constant = fit_straight1_a.GetParameter(0)

                band1a = ROOT.TBox(
                    0,
                    fit_straight1_a.GetParameter(0) - fit_straight1_a.GetParError(0),
                    500,
                    fit_straight1_a.GetParameter(0) + fit_straight1_a.GetParError(0),
                )
                band1a.SetFillColorAlpha(colours[idx], 0.3)
                band1a.SetLineColor(colours[idx])
                band_list_local.append(band1a)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                # fit straight line
                fit_straight2_a = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}2_{idx}",
                    "pol0",
                    6000,
                    9000,
                )
                fit_straight2_a.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight2_a, "N WL", "", 6000, 9000)
                constant2 = fit_straight2_a.GetParameter(0)
                fit_list.append(fit_straight2_a)
                fit_list_local.append(fit_straight2_a)

                # Create a TBox for the band
                band_a = ROOT.TBox(
                    6000,
                    fit_straight2_a.GetParameter(0) - fit_straight2_a.GetParError(0),
                    9000,
                    fit_straight2_a.GetParameter(0) + fit_straight2_a.GetParError(0),
                )
                band_a.SetFillColorAlpha(colours[idx], 0.3)  # Semi-transparent red
                band_a.SetLineColor(colours[idx])
                band_list_local.append(band_a)

                ROOT.gPad.Modified()
                ROOT.gPad.Update()
                canvas.Modified()
                canvas.Update()

                legend.AddEntry(
                    fit_straight2,
                    f"efficiency: {format(constant2/constant, '.2f')}",
                    "L",
                )
                print("eff:", constant2 / constant)
            elif j == 2:
                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, lifetime corr, {coincidences[j]} degrassed Ecut, flat fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
                # flat fit instead of Gumbel fit:
                fit_straight1_early = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel", "pol0", 0, 2000
                )
                fit_straight1_early.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_early, "N WL", "", 0, 2000)
                fit_list.append(fit_straight1_early)
                fit_list_local.append(fit_straight1_early)

                band2 = ROOT.TBox(
                    0,
                    fit_straight1_early.GetParameter(0)
                    - fit_straight1_early.GetParError(0),
                    2000,
                    fit_straight1_early.GetParameter(0)
                    + fit_straight1_early.GetParError(0),
                )
                band2.SetFillColorAlpha(colours[idx], 0.3)
                band2.SetLineColor(colours[idx])
                band_list_local.append(band2)

                fit_straight1_late = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel_late",
                    "pol0",
                    3000,
                    6000,
                )
                fit_straight1_late.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_late, "N WL", "", 3000, 6000)
                fit_list.append(fit_straight1_late)
                fit_list_local.append(fit_straight1_late)

                band3 = ROOT.TBox(
                    3000,
                    fit_straight1_late.GetParameter(0)
                    - fit_straight1_late.GetParError(0),
                    6000,
                    fit_straight1_late.GetParameter(0)
                    + fit_straight1_late.GetParError(0),
                )
                band3.SetFillColorAlpha(colours[idx], 0.3)
                band3.SetLineColor(colours[idx])
                band_list_local.append(band3)

                fit_straight1_superlate = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel_superlate",
                    "pol0",
                    8000,
                    9000,
                )
                fit_straight1_superlate.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_superlate, "N WL", "", 8000, 9000)
                fit_list.append(fit_straight1_superlate)
                fit_list_local.append(fit_straight1_superlate)

                band4 = ROOT.TBox(
                    8000,
                    fit_straight1_superlate.GetParameter(0)
                    - fit_straight1_superlate.GetParError(0),
                    9000,
                    fit_straight1_superlate.GetParameter(0)
                    + fit_straight1_superlate.GetParError(0),
                )
                band4.SetFillColorAlpha(colours[idx], 0.3)
                band4.SetLineColor(colours[idx])
                band_list_local.append(band4)

                legend.AddEntry(
                    fit_straight1_late,
                    f"efficiency: {format(fit_straight1_late.GetParameter(0)/fit_straight1_early.GetParameter(0), '.2f')}",
                    "L",
                )
            elif j == 3:
                # hist_coin_L = background_subtraction(hist_coin_L)
                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, t corr, {coincidences[j]} degrassed Ecut, flat fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )

                # flat fit instead of Gumbel fit:
                fit_straight1_early = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel", "pol0", 0, 2000
                )
                fit_straight1_early.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_early, "N WL", "", 0, 2000)
                fit_list.append(fit_straight1_early)
                fit_list_local.append(fit_straight1_early)

                band5 = ROOT.TBox(
                    0,
                    fit_straight1_early.GetParameter(0)
                    - fit_straight1_early.GetParError(0),
                    2000,
                    fit_straight1_early.GetParameter(0)
                    + fit_straight1_early.GetParError(0),
                )
                band5.SetFillColorAlpha(colours[idx], 0.3)
                band5.SetLineColor(colours[idx])
                band_list_local.append(band5)

                fit_straight1_late = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel_late",
                    "pol0",
                    5000,
                    7500,
                )
                fit_straight1_late.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_late, "N WL", "", 5000, 7500)
                fit_list.append(fit_straight1_late)
                fit_list_local.append(fit_straight1_late)

                band6 = ROOT.TBox(
                    5000,
                    fit_straight1_late.GetParameter(0)
                    - fit_straight1_late.GetParError(0),
                    7500,
                    fit_straight1_late.GetParameter(0)
                    + fit_straight1_late.GetParError(0),
                )
                band6.SetFillColorAlpha(colours[idx], 0.3)
                band6.SetLineColor(colours[idx])
                band_list_local.append(band6)

                legend.AddEntry(
                    fit_straight1_late,
                    f"efficiency: {format(fit_straight1_late.GetParameter(0)/fit_straight1_early.GetParameter(0), '.2f')}",
                    "L",
                )

            elif j == 4:
                # hist_coin_L = background_subtraction(hist_coin_L)
                hist_coin_L.SetTitle(
                    f"Coincidences L&R added, t corr, {coincidences[j]} degrassed Ecut, flat fitted"
                )
                scale_n_draw(
                    root_file[idx],
                    hist_coin_L,
                    detector_name_L,
                    rebin_fct,
                    colours[idx],
                    stack,
                    legend,
                    scaling=False,
                )
                # flat fit instead of Gumbel fit:
                fit_straight1_early = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel", "pol0", 0, 2000
                )
                fit_straight1_early.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_early, "N WL", "", 0, 2000)
                fit_list.append(fit_straight1_early)
                fit_list_local.append(fit_straight1_early)

                band7 = ROOT.TBox(
                    0,
                    fit_straight1_early.GetParameter(0)
                    - fit_straight1_early.GetParError(0),
                    2000,
                    fit_straight1_early.GetParameter(0)
                    + fit_straight1_early.GetParError(0),
                )
                band7.SetFillColorAlpha(colours[idx], 0.3)
                band7.SetLineColor(colours[idx])
                band_list_local.append(band7)

                fit_straight1_late = ROOT.TF1(
                    f"fit_straight_{coincidences[j]}_{idx}_no_gumbel_late",
                    "pol0",
                    8000,
                    9000,
                )
                fit_straight1_late.SetLineColor(colours[idx])
                hist_coin_L.Fit(fit_straight1_late, "N WL", "", 8000, 9000)
                fit_list.append(fit_straight1_late)
                fit_list_local.append(fit_straight1_late)

                band8 = ROOT.TBox(
                    8000,
                    fit_straight1_late.GetParameter(0)
                    - fit_straight1_late.GetParError(0),
                    9000,
                    fit_straight1_late.GetParameter(0)
                    + fit_straight1_late.GetParError(0),
                )
                band8.SetFillColorAlpha(colours[idx], 0.3)
                band8.SetLineColor(colours[idx])
                band_list_local.append(band8)

                legend.AddEntry(
                    fit_straight1_late,
                    f"efficiency: {format(fit_straight1_late.GetParameter(0)/fit_straight1_early.GetParameter(0), '.2f')}",
                    "L",
                )
        stack.SetTitle(
            f"Coincidences L&R added, t corr, {coincidences[j]} degrassed Ecut, flat fitted"
        )
        stack.Draw("nostack  E")
        stack.Draw("nostack  hist SAME")
        for fit in fit_list_local:
            fit.Draw("SAME")
        for band in band_list_local:
            band.Draw("SAME")

        legend.Draw()
        canvas.Update()
