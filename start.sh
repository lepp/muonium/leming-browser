#!/bin/bash


script_dir="$(dirname "$(realpath -e "${BASH_SOURCE[0]}")")"


conda run --no-capture-output --live-stream --cwd "${script_dir}" -n leming-browser "./gui.py"
