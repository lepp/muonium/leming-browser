#!/usr/bin/env -S python3 -i


import os, sys, ROOT
import configparser

import leming_data as ld

from analysis import *
import ctypes
from datetime import datetime


import re

ROOT.gROOT.SetWebDisplay()
ROOT.EnableThreadSafety()
ROOT.EnableImplicitMT()
ROOT.gEnv.SetValue("WebGui.HttpThreads", 150)
ROOT.TH1.AddDirectory(False)
ROOT.gStyle.SetOptStat("e")


###############################################
# helper functions to read out the config files
##############################################


def ReturnConfig(mainframe):
    config = mainframe.TextButtonConfig.GetText()

    if config == "":
        config = "default_config"  # replace this later with the actual default path

    return config


def ReadConfigfile(mainframe, display_warning_if_file_doesnt_exit=True):
    config_old = configparser.ConfigParser()
    used_config = ReturnConfig(mainframe)
    if used_config == "":
        used_config = "default_config"
    config_old.read(
        os.path.dirname(os.path.realpath(__file__)) + f"/configs/{used_config}" + ".ini"
    )

    print(
        "Used config",
        os.path.dirname(os.path.realpath(__file__))
        + f"/configs/{used_config}"
        + ".ini",
    )

    if (
        not os.path.exists(
            os.path.dirname(os.path.realpath(__file__))
            + "/configs/"
            + used_config
            + ".ini"
        )
    ) and display_warning_if_file_doesnt_exit:

        mainframe.not_found_box = ROOT.TGMsgBox(
            ROOT.gClient.GetRoot(),
            mainframe,
            ":(",
            "Config file doesn't exists. Choose different filename!",
            ROOT.kMBIconExclamation,
            ROOT.kMBOk,
        )
        ROOT.SetOwnership(
            mainframe.not_found_box, False
        )  # this is needed, crashes otherwise!!! https://root-forum.cern.ch/t/tgmsgbox-crashes-when-exiting-python-shell/38550/20
    else:
        pass
    hist_low_limit_dict = dict(config_old["Histogram x-axis Lower Limit"])
    for k, v in hist_low_limit_dict.items():
        hist_low_limit_dict[k] = float(v)

    hist_upper_limit_dict = dict(config_old.items("Histogram x-axis Upper Limit"))
    for k, v in hist_upper_limit_dict.items():
        hist_upper_limit_dict[k] = float(v)

    return hist_low_limit_dict, hist_upper_limit_dict


def ReadDefaultConfigfile(mainframe):
    config_old = configparser.ConfigParser()
    used_config = "default_config"
    config_old.read(
        os.path.dirname(os.path.realpath(__file__)) + f"/configs/{used_config}.ini"
    )

    hist_low_limit_dict = dict(config_old["Histogram x-axis Lower Limit"])
    for k, v in hist_low_limit_dict.items():
        hist_low_limit_dict[k] = float(v)

    hist_upper_limit_dict = dict(config_old.items("Histogram x-axis Upper Limit"))
    for k, v in hist_upper_limit_dict.items():
        hist_upper_limit_dict[k] = float(v)

    return hist_low_limit_dict, hist_upper_limit_dict


def ReturnConfigName(mainframe):
    config_name = mainframe.TextButtonConfigNew.GetText()

    if config_name == "":
        config_name = "config_" + str(datetime.now().isoformat())
    return config_name


#######################################
# main TGMainFrame class
#######################################


class pMainFrame(ROOT.TGMainFrame):
    def __init__(self, parent, width, height):
        ROOT.TGMainFrame.__init__(self, parent, width, height)

        ####################
        # define frames
        ###################

        self.IntroFrame = ROOT.TGVerticalFrame(self, 200, 400)

        self.fHf = ROOT.TGHorizontalFrame(
            self,
            width,
            height,
            ROOT.kFixedHeight | ROOT.kLHintsExpandY,
        )

        self.LeftFrame = ROOT.TGVerticalFrame(
            self.fHf,
            width,
            height,
            ROOT.kChildFrame | ROOT.kLHintsExpandX,
        )
        self.RightFrame = ROOT.TGVerticalFrame(
            self.fHf, int(0.3 * width), height, ROOT.kChildFrame
        )

        self.GroupFrame = ROOT.TGGroupFrame(
            self, "Enter dataset 1", ROOT.kHorizontalFrame
        )

        self.GroupFrame2 = ROOT.TGGroupFrame(
            self, "Enter dataset 2 (optional)", ROOT.kHorizontalFrame
        )

        self.ButtonsFrame = ROOT.TGGroupFrame(
            self.LeftFrame, "Select histogram", ROOT.kChildFrame | ROOT.kVerticalFrame
        )

        self.ButtonsFrame2 = ROOT.TGGroupFrame(
            self.LeftFrame, "Select histogram", ROOT.kChildFrame | ROOT.kVerticalFrame
        )
        self.ButtonsFrame3 = ROOT.TGGroupFrame(
            self.LeftFrame, "Select histogram", ROOT.kChildFrame | ROOT.kVerticalFrame
        )
        self.ButtonsFrame4 = ROOT.TGGroupFrame(
            self.LeftFrame, "Select histogram", ROOT.kChildFrame | ROOT.kVerticalFrame
        )

        self.ConfigFrame = ROOT.TGGroupFrame(
            self.RightFrame,
            "Select configuration",
            ROOT.kChildFrame | ROOT.kVerticalFrame,
        )
        self.RightSubFrame = ROOT.TGHorizontalFrame(
            self.ConfigFrame, int(0.3 * width), height, ROOT.kChildFrame
        )

        self.RightSubFrame2 = ROOT.TGVerticalFrame(
            self.ConfigFrame, int(0.3 * width), height, ROOT.kChildFrame
        )

        self.fHf_fun = ROOT.TGHorizontalFrame(self, 20, 20)
        self.fHf_fun_V1 = ROOT.TGVerticalFrame(self.fHf_fun, 7, 7, ROOT.kChildFrame)

        self.TestFrame = ROOT.TGGroupFrame(self.fHf_fun_V1, "", ROOT.kHorizontalFrame)
        self.FunFrame = ROOT.TGVerticalFrame(self.fHf_fun, 7, 7)

        #####################

        # DO NOT change the order of the list (or if you do, update the E cut special case for Electron Energy Left and Right in def DrawHists  )
        self.histogram_list = [
            "Muon Energy",
            "Efficiency and flat fit",
            "Efficiency and Gumbel",
            "Coincidences L+R",
            "Coincidences Left",
            "Coincidences Right",
            "Coincidences Cross",
            "Electron Energy Left",
            "Electron Energy Right",
            "Electron Time Left",
            "Electron Time Right",
            "Asymmetry",
            "Atomic Electron Energy",
            "Energy of Triple Coin",
        ]
        ###########################
        # histo 1
        #############################

        self.m_all_draw = ROOT.TPyDispatcher(self.GetSelectedHist)

        self.fLcombo = ROOT.TGLayoutHints(
            ROOT.kLHintsTop | ROOT.kLHintsLeft, 5, 5, 5, 5
        )

        # Combo box widget
        self.fCombo = ROOT.TGComboBox(self.ButtonsFrame, 100)
        for i in range(len(self.histogram_list)):
            tmp = self.histogram_list[i]
            self.fCombo.AddEntry(tmp, i)

        self.fCombo.Resize(150, 20)
        self.fCombo.SetName("MyComboBox")

        self.ButtonsFrame.AddFrame(self.fCombo, self.fLcombo)

        self.fCombo.Connect(
            "Selected(Int_t)", "TPyDispatcher", self.m_all_draw, "Dispatch()"
        )

        ###################################
        # checkboxes to select options
        ####################################

        # lifetime correction
        self.TCorrOption = ROOT.TGCheckButton(
            self.ButtonsFrame, "Lifetime corrected", 1
        )
        self.TCorrOption.SetState(ROOT.kButtonUp)
        self.ButtonsFrame.AddFrame(self.TCorrOption, ROOT.TGLayoutHints())

        # bkg sub
        self.BkgSubOption = ROOT.TGCheckButton(
            self.ButtonsFrame, "Background subtracted", 1
        )
        self.BkgSubOption.SetState(ROOT.kButtonUp)
        self.ButtonsFrame.AddFrame(self.BkgSubOption, ROOT.TGLayoutHints())

        # degras
        self.DegrasOption = ROOT.TGCheckButton(self.ButtonsFrame, "Degrased", 1)
        self.DegrasOption.SetState(ROOT.kButtonUp)
        self.ButtonsFrame.AddFrame(self.DegrasOption, ROOT.TGLayoutHints())

        # E cut
        self.EcutOption = ROOT.TGCheckButton(self.ButtonsFrame, "with E cuts", 1)
        self.EcutOption.SetState(ROOT.kButtonUp)
        self.ButtonsFrame.AddFrame(self.EcutOption, ROOT.TGLayoutHints())

        ###########################
        # histo 2
        #############################

        self.m_all_draw = ROOT.TPyDispatcher(self.GetSelectedHist2)

        self.fLcombo2 = ROOT.TGLayoutHints(
            ROOT.kLHintsTop | ROOT.kLHintsLeft, 5, 5, 5, 5
        )

        # Combo box widget
        self.fCombo2 = ROOT.TGComboBox(self.ButtonsFrame2, 100)
        for i in range(len(self.histogram_list)):
            tmp = self.histogram_list[i]
            self.fCombo2.AddEntry(tmp, i)

        self.fCombo2.Resize(150, 20)
        self.fCombo2.SetName("MyComboBox")

        self.ButtonsFrame2.AddFrame(self.fCombo2, self.fLcombo2)

        self.fCombo2.Connect(
            "Selected(Int_t)", "TPyDispatcher", self.m_all_draw, "Dispatch()"
        )

        ###################################
        # checkboxes to select options
        ####################################

        # lifetime correction
        self.TCorrOption2 = ROOT.TGCheckButton(
            self.ButtonsFrame2, "Lifetime corrected", 1
        )
        self.TCorrOption2.SetState(ROOT.kButtonUp)
        self.ButtonsFrame2.AddFrame(self.TCorrOption2, ROOT.TGLayoutHints())

        # bkg sub
        self.BkgSubOption2 = ROOT.TGCheckButton(
            self.ButtonsFrame2, "Background subtracted", 1
        )
        self.BkgSubOption2.SetState(ROOT.kButtonUp)
        self.ButtonsFrame2.AddFrame(self.BkgSubOption2, ROOT.TGLayoutHints())

        # degras
        self.DegrasOption2 = ROOT.TGCheckButton(self.ButtonsFrame2, "Degrased", 1)
        self.DegrasOption2.SetState(ROOT.kButtonUp)
        self.ButtonsFrame2.AddFrame(self.DegrasOption2, ROOT.TGLayoutHints())

        # E cut
        self.EcutOption2 = ROOT.TGCheckButton(self.ButtonsFrame2, "with E cut", 1)
        self.EcutOption2.SetState(ROOT.kButtonUp)
        self.ButtonsFrame2.AddFrame(self.EcutOption2, ROOT.TGLayoutHints())

        ###########################
        # histo 3
        #############################

        self.m_all_draw = ROOT.TPyDispatcher(self.GetSelectedHist3)

        self.fLcombo3 = ROOT.TGLayoutHints(
            ROOT.kLHintsTop | ROOT.kLHintsLeft, 5, 5, 5, 5
        )

        # Combo box widget
        self.fCombo3 = ROOT.TGComboBox(self.ButtonsFrame3, 100)
        for i in range(len(self.histogram_list)):
            tmp = self.histogram_list[i]
            self.fCombo3.AddEntry(tmp, i)

        self.fCombo3.Resize(150, 20)
        self.fCombo3.SetName("MyComboBox")

        self.ButtonsFrame3.AddFrame(self.fCombo3, self.fLcombo3)

        self.fCombo3.Connect(
            "Selected(Int_t)", "TPyDispatcher", self.m_all_draw, "Dispatch()"
        )

        ###################################
        # checkboxes to select options
        ####################################

        # lifetime correction
        self.TCorrOption3 = ROOT.TGCheckButton(
            self.ButtonsFrame3, "Lifetime corrected", 1
        )
        self.TCorrOption3.SetState(ROOT.kButtonUp)
        self.ButtonsFrame3.AddFrame(self.TCorrOption3, ROOT.TGLayoutHints())

        # bkg sub
        self.BkgSubOption3 = ROOT.TGCheckButton(
            self.ButtonsFrame3, "Background subtracted", 1
        )
        self.BkgSubOption3.SetState(ROOT.kButtonUp)
        self.ButtonsFrame3.AddFrame(self.BkgSubOption3, ROOT.TGLayoutHints())

        # degras
        self.DegrasOption3 = ROOT.TGCheckButton(self.ButtonsFrame3, "Degrased", 1)
        self.DegrasOption3.SetState(ROOT.kButtonUp)
        self.ButtonsFrame3.AddFrame(self.DegrasOption3, ROOT.TGLayoutHints())

        # E cut
        self.EcutOption3 = ROOT.TGCheckButton(self.ButtonsFrame3, "with E cut", 1)
        self.EcutOption3.SetState(ROOT.kButtonUp)
        self.ButtonsFrame3.AddFrame(self.EcutOption3, ROOT.TGLayoutHints())

        ###########################
        # histo 4
        #############################

        self.m_all_draw = ROOT.TPyDispatcher(self.GetSelectedHist4)

        self.fLcombo4 = ROOT.TGLayoutHints(
            ROOT.kLHintsTop | ROOT.kLHintsLeft, 5, 5, 5, 5
        )

        # Combo box widget
        self.fCombo4 = ROOT.TGComboBox(self.ButtonsFrame4, 100)
        for i in range(len(self.histogram_list)):
            tmp = self.histogram_list[i]
            self.fCombo4.AddEntry(tmp, i)

        self.fCombo4.Resize(150, 20)
        self.fCombo4.SetName("MyComboBox")

        self.ButtonsFrame4.AddFrame(self.fCombo4, self.fLcombo4)

        self.fCombo4.Connect(
            "Selected(Int_t)", "TPyDispatcher", self.m_all_draw, "Dispatch()"
        )

        ###################################
        # checkboxes to select options
        ####################################

        # lifetime correction
        self.TCorrOption4 = ROOT.TGCheckButton(
            self.ButtonsFrame4, "Lifetime corrected", 1
        )
        self.TCorrOption4.SetState(ROOT.kButtonUp)
        self.ButtonsFrame4.AddFrame(self.TCorrOption4, ROOT.TGLayoutHints())

        # bkg sub
        self.BkgSubOption4 = ROOT.TGCheckButton(
            self.ButtonsFrame4, "Background subtracted", 1
        )
        self.BkgSubOption4.SetState(ROOT.kButtonUp)
        self.ButtonsFrame4.AddFrame(self.BkgSubOption4, ROOT.TGLayoutHints())

        # degras
        self.DegrasOption4 = ROOT.TGCheckButton(self.ButtonsFrame4, "Degrased", 1)
        self.DegrasOption4.SetState(ROOT.kButtonUp)
        self.ButtonsFrame4.AddFrame(self.DegrasOption4, ROOT.TGLayoutHints())

        # E cut
        self.EcutOption4 = ROOT.TGCheckButton(self.ButtonsFrame4, "with E cut", 1)
        self.EcutOption4.SetState(ROOT.kButtonUp)
        self.ButtonsFrame4.AddFrame(self.EcutOption4, ROOT.TGLayoutHints())

        #####################################################################
        # button to use  config file
        #####################################################################
        self.UseOwnConfigButton = ROOT.TGCheckButton(
            self.RightSubFrame, "Use own config file.", 1
        )
        self.UseOwnConfigButton.SetState(ROOT.kButtonUp)

        self.RightSubFrame.AddFrame(self.UseOwnConfigButton, ROOT.TGLayoutHints())

        ########################
        # textfield for config file
        ########################

        self.fLabel = ROOT.TGLabel(self.RightSubFrame, "Config file name:")
        self.RightSubFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBuffer = ROOT.TGTextBuffer(20)
        self.TextButtonConfig = ROOT.TGTextEntry(self.RightSubFrame, self.TextBuffer)
        self.RightSubFrame.AddFrame(self.TextButtonConfig, ROOT.TGLayoutHints())

        #####################################################################
        # button to use default config file
        #####################################################################
        self.UseConfigButton = ROOT.TGCheckButton(
            self.RightSubFrame2, "Use default config file", 1
        )
        self.UseConfigButton.SetState(ROOT.kButtonUp)

        self.RightSubFrame2.AddFrame(self.UseConfigButton, ROOT.TGLayoutHints())

        #################################
        # draw all selected hists
        ################################
        self.m_all = ROOT.TPyDispatcher(self.DrawHists)
        self.DrawSelectedButton = ROOT.TGTextButton(
            self.RightSubFrame2, "Draw selected histograms", 10
        )
        self.DrawSelectedButton.Connect(
            "Clicked()", "TPyDispatcher", self.m_all, "Dispatch()"
        )
        self.RightSubFrame2.AddFrame(self.DrawSelectedButton, ROOT.TGLayoutHints())

        ####################################
        # explanantion text label
        ###################################
        self.fLabel = ROOT.TGLabel(self.IntroFrame, "Welcome to the LEMING Browser!")
        self.IntroFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())

        self.fLabel2 = ROOT.TGLabel(
            self.IntroFrame,
            "Enter the path to where your data is stored below, e.g. ~/path/to/my/files. If no path is specified, the default path is chosen.",
        )
        self.IntroFrame.AddFrame(self.fLabel2, ROOT.TGLayoutHints())

        self.fLabel2 = ROOT.TGLabel(
            self.IntroFrame,
            "Runs can be a comma-separated list of run numbers and run ranges (e.g. 3,4,100-200,300-400,1000), or if the files are already merged to a file (e.g. 1316-1322.root), give its full name including the .root suffix.",
        )
        self.IntroFrame.AddFrame(self.fLabel2, ROOT.TGLayoutHints())

        self.fLabel3 = ROOT.TGLabel(
            self.IntroFrame,
            "The dataset name which should be used in the legend of the histograms can be chosen. If no name is specified, the run range is used.",
        )
        self.IntroFrame.AddFrame(self.fLabel3, ROOT.TGLayoutHints())

        ########################
        # textfield for path
        ########################
        # text entry to enter path
        self.TextButtonTitle = "Enter path:"

        self.fLabel = ROOT.TGLabel(self.GroupFrame, self.TextButtonTitle)
        self.GroupFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBuffer = ROOT.TGTextBuffer(20)
        self.TextButton = ROOT.TGTextEntry(self.GroupFrame, self.TextBuffer)
        self.GroupFrame.AddFrame(self.TextButton, ROOT.TGLayoutHints())

        #############
        # filebrowser
        ############
        self.fo = ROOT.TPyDispatcher(self.BrowseFiles)

        self.filebrowser = ROOT.TPyDispatcher(self.BrowseFiles)
        self.GetFBButton = ROOT.TGTextButton(self.GroupFrame, "Browse Folders")
        self.GetFBButton.Connect(
            "Clicked()",
            "TPyDispatcher",
            self.fo,
            "Dispatch()",
        )
        self.GroupFrame.AddFrame(self.GetFBButton, ROOT.TGLayoutHints())

        ###############################
        # textfield for runs
        ############################
        # text entry to enter runs
        self.TextButtonTitleRuns = "Enter runs:"

        self.fLabel = ROOT.TGLabel(self.GroupFrame, self.TextButtonTitleRuns)
        self.GroupFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBufferRuns = ROOT.TGTextBuffer(20)
        self.TextButtonRuns = ROOT.TGTextEntry(self.GroupFrame, self.TextBufferRuns)
        self.GroupFrame.AddFrame(self.TextButtonRuns, ROOT.TGLayoutHints())

        ###############################
        # textfield for legend
        ############################
        # text entry to enter legend
        self.TextButtonTitleRunsLegend = "Enter name:"

        self.fLabel = ROOT.TGLabel(self.GroupFrame, self.TextButtonTitleRunsLegend)
        self.GroupFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())

        self.TextBufferLegend = ROOT.TGTextBuffer(20)
        self.TextButtonLegend = ROOT.TGTextEntry(self.GroupFrame, self.TextBufferLegend)
        self.GroupFrame.AddFrame(self.TextButtonLegend, ROOT.TGLayoutHints())

        #################################
        # textfield for Cooldown
        ###################################

        self.fLabel = ROOT.TGLabel(self.GroupFrame, "Use E cuts of Cooldown number:")
        self.GroupFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBuffer = ROOT.TGTextBuffer(20)
        self.TextButtonCD = ROOT.TGTextEntry(self.GroupFrame, self.TextBuffer)
        self.GroupFrame.AddFrame(self.TextButtonCD, ROOT.TGLayoutHints())

        ########################
        # textfield for path for dataset 2
        ########################
        # text entry to enter path
        self.TextBuffer2 = ROOT.TGTextBuffer(20)
        self.fLabel = ROOT.TGLabel(self.GroupFrame2, self.TextButtonTitle)
        self.GroupFrame2.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextButton2 = ROOT.TGTextEntry(self.GroupFrame2, self.TextBuffer2)
        self.GroupFrame2.AddFrame(self.TextButton2, ROOT.TGLayoutHints())

        #############
        # filebrowser
        ############
        self.fo = ROOT.TPyDispatcher(self.BrowseFiles2)

        self.filebrowser = ROOT.TPyDispatcher(self.BrowseFiles2)
        self.GetFBButton2 = ROOT.TGTextButton(self.GroupFrame2, "Browse Folders")

        self.GetFBButton2.Connect(
            "Clicked()",
            "TPyDispatcher",
            self.fo,
            "Dispatch()",
        )
        self.GroupFrame2.AddFrame(self.GetFBButton2, ROOT.TGLayoutHints())

        ###############################
        # textfield for runs for dataset 2
        ############################
        # text entry to enter runs
        self.fLabel = ROOT.TGLabel(self.GroupFrame2, self.TextButtonTitleRuns)
        self.GroupFrame2.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBufferRuns2 = ROOT.TGTextBuffer(20)
        self.TextButtonRuns2 = ROOT.TGTextEntry(self.GroupFrame2, self.TextBufferRuns2)
        # self.TextButtonRuns2.SetTitle(self.TextButtonTitleRuns)
        self.GroupFrame2.AddFrame(self.TextButtonRuns2, ROOT.TGLayoutHints())

        ###############################
        # textfield for legend for dataset 2
        ############################
        # text entry to enter legend
        self.TextButtonTitleRuns = "Enter name:"

        self.fLabel = ROOT.TGLabel(self.GroupFrame2, self.TextButtonTitleRuns)
        self.GroupFrame2.AddFrame(self.fLabel, ROOT.TGLayoutHints())

        self.TextBufferLegend2 = ROOT.TGTextBuffer(20)
        self.TextButtonLegend2 = ROOT.TGTextEntry(
            self.GroupFrame2, self.TextBufferLegend2
        )
        self.GroupFrame2.AddFrame(self.TextButtonLegend2, ROOT.TGLayoutHints())

        #################################
        # textfield for Cooldown
        ###################################

        self.fLabel = ROOT.TGLabel(self.GroupFrame2, "Use E cuts of Cooldown number:")
        self.GroupFrame2.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBuffer = ROOT.TGTextBuffer(20)
        self.TextButtonCD2 = ROOT.TGTextEntry(self.GroupFrame2, self.TextBuffer)
        self.GroupFrame2.AddFrame(self.TextButtonCD2, ROOT.TGLayoutHints())

        ###############################
        # textfield for name of config file
        ############################
        # text entry to enter name of config file
        self.TextButtonTitleConfig = "Name of new config file:"

        self.fLabel = ROOT.TGLabel(self.TestFrame, self.TextButtonTitleConfig)
        self.TestFrame.AddFrame(self.fLabel, ROOT.TGLayoutHints())
        self.TextBufferRuns = ROOT.TGTextBuffer(20)
        self.TextButtonConfigNew = ROOT.TGTextEntry(self.TestFrame, self.TextBufferRuns)
        # self.TextButtonConfigNew.SetTitle(self.TextButtonTitleRuns)
        self.TestFrame.AddFrame(self.TextButtonConfigNew, ROOT.TGLayoutHints())

        #######################
        # save config button
        ######################
        self.Save = ROOT.TPyDispatcher(self.Save)

        self.SaveButton = ROOT.TGTextButton(self.TestFrame, "&Save Config", 50)
        self.SaveButton.Connect("Clicked()", "TPyDispatcher", self.Save, "Dispatch()")

        self.TestFrame.AddFrame(self.SaveButton, ROOT.TGLayoutHints(ROOT.kLHintsBottom))

        #################
        # exit button
        ################
        self.ExitButton = ROOT.TGTextButton(self.TestFrame, "&Exit", 50)
        self.ExitButton.Connect(
            "Clicked()", "TApplication", ROOT.gApplication, "Terminate()"
        )
        self.TestFrame.AddFrame(self.ExitButton, ROOT.TGLayoutHints(ROOT.kLHintsBottom))

        ###################
        # try to include a leming cartoon
        ###################
        self.ipic = ROOT.gClient.GetPicture("images/lemming-image.jpg")
        if not self.ipic:
            print("Failed to load image")

        self.icon = ROOT.TGIcon(self.FunFrame, self.ipic, 1, 1)

        self.FunFrame.AddFrame(
            self.icon,
            ROOT.TGLayoutHints(ROOT.kLHintsRight | ROOT.kLHintsBottom, 1, 7, 1, 1),
            # ROOT.TGLayoutHints(ROOT.kLHintsRight, 1, 7, 1, 1),
        )

        ###########################
        # add frames to TGMainFrame
        ##########################
        self.AddFrame(self.IntroFrame, ROOT.TGLayoutHints(ROOT.kLHintsExpandX))

        self.AddFrame(self.GroupFrame, ROOT.TGLayoutHints(ROOT.kLHintsExpandX))
        self.AddFrame(self.GroupFrame2, ROOT.TGLayoutHints(ROOT.kLHintsExpandX))

        self.fHf.AddFrame(
            self.LeftFrame,
            ROOT.TGLayoutHints(
                ROOT.kLHintsLeft | ROOT.kLHintsExpandX | ROOT.kLHintsExpandY
            ),
        )
        self.fHf.AddFrame(
            self.RightFrame,
            ROOT.TGLayoutHints(
                ROOT.kLHintsRight | ROOT.kLHintsExpandX | ROOT.kLHintsExpandY
            ),
        )
        self.LeftFrame.AddFrame(
            self.ButtonsFrame,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )

        self.LeftFrame.AddFrame(
            self.ButtonsFrame2,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )
        self.LeftFrame.AddFrame(
            self.ButtonsFrame3,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )
        self.LeftFrame.AddFrame(
            self.ButtonsFrame4,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )
        self.ConfigFrame.AddFrame(
            self.RightSubFrame,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )
        self.ConfigFrame.AddFrame(
            self.RightSubFrame2,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )
        self.RightFrame.AddFrame(
            self.ConfigFrame,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX, 2, 2, 2, 2),
        )

        self.AddFrame(
            self.fHf,
            ROOT.TGLayoutHints(
                ROOT.kLHintsRight | ROOT.kLHintsExpandX | ROOT.kLHintsExpandY
            ),
        )

        self.AddFrame(
            self.fHf_fun,
            ROOT.TGLayoutHints(
                ROOT.kLHintsRight
                | ROOT.kLHintsBottom
                | ROOT.kLHintsExpandX
                # | ROOT.kLHintsExpandY
            ),
        )
        self.fHf_fun.AddFrame(
            self.fHf_fun_V1,
            ROOT.TGLayoutHints(
                ROOT.kLHintsLeft
                | ROOT.kLHintsExpandX
                # | ROOT.kLHintsExpandY
                | ROOT.kLHintsBottom
            ),
        )
        self.fHf_fun_V1.AddFrame(
            self.TestFrame,
            ROOT.TGLayoutHints(ROOT.kLHintsExpandX | ROOT.kLHintsBottom, 2, 2, 2, 2),
        )

        self.fHf_fun.AddFrame(
            self.FunFrame,
            ROOT.TGLayoutHints(
                ROOT.kLHintsRight | ROOT.kLHintsExpandX | ROOT.kLHintsExpandY
            ),
        )

        self.SetWindowName("LEMING Browser")
        self.MapSubwindows()
        # self.Resize(self.GetDefaultSize())
        self.Layout()

        self.MapWindow()

        # define function that belongs to hist name
        self.HistnameFunctionPairs = [
            ("Muon Energy", muon_energy),
            (
                "Efficiency and flat fit",
                coincidence_LR_added_degrassed_ecut_with_eff_fit_flat,
            ),
            ("Efficiency and Gumbel", coincidence_LR_added_degrassed_ecut_with_eff_fit),
            ("Coincidences L+R", coin_LR_options),
            ("Coincidences Left", coin_L_options),
            ("Coincidences Right", coin_R_options),
            ("Coincidences Cross", coin_cross_options),
            ("Electron Energy Left", electronenergy_left),
            ("Electron Energy Right", electronenergy_right),
            ("Electron Time Left", electrontime_left),
            ("Electron Time Right", electrontime_right),
            ("Asymmetry", coincidence_LR_assy),
            ("Atomic Electron Energy", atomic_energy),
            ("Energy of Triple Coin", coincidence_LR_added_triple_coin),
        ]

    ##########################
    # define the config file
    ########################
    def Save(self):

        config = configparser.ConfigParser()
        config["General"] = {}
        config["Histogram x-axis Lower Limit"] = {}
        config["Histogram x-axis Upper Limit"] = {}
        config["Histogram x-axis Log scale"] = {}

        config["General"]["Time"] = str(datetime.now().isoformat())
        config["General"]["Path dataset 1"] = str(self.path)
        config["General"]["Runs dataset 1"] = str(self.runs)
        config["General"]["Name dataset 1"] = str(self.legend)

        config["General"]["Path dataset 2"] = str(self.path2)
        config["General"]["Runs dataset 2"] = str(self.runs2)
        config["General"]["Name dataset 2"] = str(self.legend2)
        config["General"]["Used E cuts left [F N], dataset 1"] = str(ecut_l[self.CD1])
        config["General"]["Used E cuts right [F N], dataset 1"] = str(ecut_r[self.CD1])
        config["General"]["Used E cuts left [F N], dataset 2"] = str(ecut_l[self.CD2])
        config["General"]["Used E cuts right [F N], dataset 2"] = str(ecut_r[self.CD2])

        for key, item in self.root_file[0].stacks.items():
            i = 0
            for stack_item in item:
                config["Histogram x-axis Lower Limit"][key + str(i)] = str(
                    stack_item.GetXaxis().GetBinLowEdge(
                        stack_item.GetXaxis().GetFirst()
                    )
                )
                config["Histogram x-axis Upper Limit"][key + str(i)] = str(
                    stack_item.GetXaxis().GetBinUpEdge(stack_item.GetXaxis().GetLast())
                )

                i = i + 1

        old_config_file_low_dict, old_config_file_high_dict = ReadConfigfile(
            self, display_warning_if_file_doesnt_exit=False
        )
        for key, value in old_config_file_low_dict.items():
            if key not in config["Histogram x-axis Lower Limit"]:
                config["Histogram x-axis Lower Limit"][key] = str(value)
            else:
                pass
        for key, value in old_config_file_high_dict.items():
            if key not in config["Histogram x-axis Upper Limit"]:
                config["Histogram x-axis Upper Limit"][key] = str(value)
            else:
                pass

        # Write to a file
        config_name_new = ReturnConfigName(self)

        if os.path.exists(
            os.path.dirname(os.path.realpath(__file__))
            + "/configs/"
            + config_name_new
            + ".ini"
        ):

            self.warning = ROOT.TGMsgBox(
                ROOT.gClient.GetRoot(),
                self,
                ":(",
                "File exists already. Choose different filename!",
                ROOT.kMBIconExclamation,
                ROOT.kMBOk,
            )
            ROOT.SetOwnership(
                self.warning, False
            )  # this is needed, crashes otherwise!!! https://root-forum.cern.ch/t/tgmsgbox-crashes-when-exiting-python-shell/38550/20
        else:

            with open(
                os.path.dirname(os.path.realpath(__file__))
                + f"/configs/{config_name_new}.ini",
                "w",
            ) as configfile:
                config.write(configfile)

    def OpenFiles(self):

        self.path, self.path2 = self.ReturnPaths()
        print("paths:", self.path)
        print("path2:", self.path2)

        self.runs, self.runs2 = self.ReturnRunNumbers()
        print("runs", self.runs)
        print("runs 2nd dataset", self.runs2)

        self.legend, self.legend2 = self.ReturnLegends()
        print("name of dataset", self.legend)
        print("name of dataset 2", self.legend2)

        if self.legend == "":
            self.legend = self.runs
        if self.legend2 == "":
            self.legend2 = self.runs2
        if self.runs2 != "":
            name = ld.LDlist(
                [self.path, self.runs, self.legend],
                [self.path2, self.runs2, self.legend2],
            )
        else:
            if (
                ("-" in self.runs)
                or ("," in self.runs)
                or any(char.isalpha() for char in self.runs)
            ):
                name = ld.LDlist([self.path, self.runs, self.legend])
            else:
                name = ld.LDlist(
                    [self.path, "output0" + self.runs + ".root", self.legend]
                )

        root_file = name.data_list

        return root_file

    def BrowseFiles(self):
        # stolen from https://root-forum.cern.ch/t/tgfiledialog-in-pyroot/15936
        global window
        fi = ROOT.TGFileInfo()

        file_dialog = ROOT.TGFileDialog(ROOT.gClient.GetRoot(), window, ROOT.kDOpen, fi)
        ROOT.SetOwnership(file_dialog, False)

        fullpath = fi.fFilename
        print("selected folder", fullpath)
        fullpath = str(fullpath)

        self.TextButton.SetText(fullpath)

        del file_dialog

    def BrowseFiles2(self):
        # stolen from https://root-forum.cern.ch/t/tgfiledialog-in-pyroot/15936
        global window
        fi = ROOT.TGFileInfo()

        file_dialog = ROOT.TGFileDialog(ROOT.gClient.GetRoot(), window, ROOT.kDOpen, fi)
        ROOT.SetOwnership(file_dialog, False)

        fullpath = fi.fFilename
        print("selected folder", fullpath)
        fullpath = str(fullpath)

        self.TextButton2.SetText(fullpath)

        del file_dialog

    def ReturnPaths(self):
        path = self.TextButton.GetText()

        if path == "":
            path = ld.default_data_location

        path2 = self.TextButton2.GetText()

        if path2 == "":
            path2 = ld.default_data_location
        return path, path2

    def ReturnRunNumbers(self):
        runs = self.TextButtonRuns.GetText()

        runs2 = self.TextButtonRuns2.GetText()
        return runs, runs2

    def ReturnLegends(self):
        legend = self.TextButtonLegend.GetText()

        legend2 = self.TextButtonLegend2.GetText()
        return legend, legend2

    def ReturnCooldownNumbers(self):
        CD1 = self.TextButtonCD.GetText()
        CD2 = self.TextButtonCD2.GetText()
        if CD1 == "":
            CD1 = str(0)
        if CD2 == "":
            CD2 = str(0)
        CD1 = int(CD1) - 1
        CD2 = int(CD2) - 1

        return CD1, CD2

    def GetSelectedHist(self):
        selected_id = self.fCombo.GetSelected()
        return selected_id

    def GetSelectedHist2(self):
        selected_id = self.fCombo2.GetSelected()
        return selected_id

    def GetSelectedHist3(self):
        selected_id = self.fCombo3.GetSelected()
        return selected_id

    def GetSelectedHist4(self):
        selected_id = self.fCombo4.GetSelected()
        return selected_id

    def DrawHists(self):
        # read in data
        self.root_file = self.OpenFiles()

        self.CD1, self.CD2 = self.ReturnCooldownNumbers()

        selected_id_list = [
            self.GetSelectedHist(),
            self.GetSelectedHist2(),
            self.GetSelectedHist3(),
            self.GetSelectedHist4(),
        ]
        TCorrOption_button_list = [
            self.TCorrOption,
            self.TCorrOption2,
            self.TCorrOption3,
            self.TCorrOption4,
        ]
        BkgSubOption_button_list = [
            self.BkgSubOption,
            self.BkgSubOption2,
            self.BkgSubOption3,
            self.BkgSubOption4,
        ]
        DegrasOption_button_list = [
            self.DegrasOption,
            self.DegrasOption2,
            self.DegrasOption3,
            self.DegrasOption4,
        ]
        EcutOption_button_list = [
            self.EcutOption,
            self.EcutOption2,
            self.EcutOption3,
            self.EcutOption4,
        ]

        for i in range(len(selected_id_list)):
            selected_id = selected_id_list[i]
            print("selected id", selected_id)
            if selected_id != -1 and selected_id != 7 and selected_id != 8 and selected_id !=1 and selected_id != 2: 
                option_states = {
                    "tcorr": TCorrOption_button_list[i].IsDown(),
                    "degras": DegrasOption_button_list[i].IsDown(),
                    "ecut":( 
                            [self.CD1, self.CD2]
                            if EcutOption_button_list[i].IsDown()
                            else [-2, -2]
                    ),
                    "bkg_sub": BkgSubOption_button_list[i].IsDown(),
                }
                for histname, function in self.HistnameFunctionPairs:
                    if self.histogram_list[selected_id] == histname:
                        function(
                            self.root_file,
                            tcorr=option_states["tcorr"],
                            degras=option_states["degras"],
                            ecut=option_states["ecut"],
                            bkg_sub=option_states["bkg_sub"],
                        )

                if self.UseConfigButton.IsDown():
                    low_dict, high_dict = ReadDefaultConfigfile(self)

                    for key, item in self.root_file[0].stacks.items():

                        if key + str(i) in low_dict:
                            for stack_item in item:
                                i = 0
                                if len(item) > 1:
                                    self.root_file[0].cans[key][0].cd(i + 1)
                                else:
                                    self.root_file[0].cans[key][0].cd(0)

                                stack_item.GetXaxis().SetRangeUser(
                                    low_dict[key + str(i)], high_dict[key + str(i)]
                                )

                                ROOT.gPad.Modified()
                                ROOT.gPad.Update()
                                i = i + 1
                elif self.UseOwnConfigButton.IsDown():
                    low_dict, high_dict = ReadConfigfile(self)

                    for key, item in self.root_file[0].stacks.items():
                        if key + str(i) in low_dict:

                            i = 0
                            for stack_item in item:
                                if len(item) > 1:
                                    self.root_file[0].cans[key][0].cd(i + 1)
                                else:
                                    self.root_file[0].cans[key][0].cd(0)

                                try:
                                    stack_item.GetXaxis().SetRangeUser(
                                        low_dict[key + str(i)], high_dict[key + str(i)]
                                    )

                                except:
                                    pass  # if histogram name not in config file, do nothing

                                ROOT.gPad.Modified()
                                ROOT.gPad.Update()
                                i = i + 1
            elif selected_id != -1 and (selected_id == 7 or selected_id == 8 or selected_id == 1 or selected_id == 2):
                option_states = {
                    "tcorr": TCorrOption_button_list[i].IsDown(),
                    "degras": DegrasOption_button_list[i].IsDown(),
                    "ecut": ([self.CD1, self.CD2]),
                    "bkg_sub": BkgSubOption_button_list[i].IsDown(),
                }
                for histname, function in self.HistnameFunctionPairs:
                    if self.histogram_list[selected_id] == histname:
                        function(
                            self.root_file,
                            tcorr=option_states["tcorr"],
                            degras=option_states["degras"],
                            ecut=option_states["ecut"],
                            bkg_sub=option_states["bkg_sub"],
                        )

                if self.UseConfigButton.IsDown():
                    low_dict, high_dict = ReadDefaultConfigfile(self)

                    for key, item in self.root_file[0].stacks.items():

                        if key + str(i) in low_dict:
                            for stack_item in item:
                                i = 0
                                if len(item) > 1:
                                    self.root_file[0].cans[key][0].cd(i + 1)
                                else:
                                    self.root_file[0].cans[key][0].cd(0)

                                stack_item.GetXaxis().SetRangeUser(
                                    low_dict[key + str(i)], high_dict[key + str(i)]
                                )

                                ROOT.gPad.Modified()
                                ROOT.gPad.Update()
                                i = i + 1
                elif self.UseOwnConfigButton.IsDown():
                    low_dict, high_dict = ReadConfigfile(self)

                    for key, item in self.root_file[0].stacks.items():
                        if key + str(i) in low_dict:

                            i = 0
                            for stack_item in item:
                                if len(item) > 1:
                                    self.root_file[0].cans[key][0].cd(i + 1)
                                else:
                                    self.root_file[0].cans[key][0].cd(0)

                                try:
                                    stack_item.GetXaxis().SetRangeUser(
                                        low_dict[key + str(i)], high_dict[key + str(i)]
                                    )

                                except:
                                    pass  # if histogram name not in config file, do nothing

                                ROOT.gPad.Modified()
                                ROOT.gPad.Update()
                                i = i + 1

    def __del__(self):
        self.Cleanup()


if __name__ == "__main__":

    window = pMainFrame(ROOT.gClient.GetRoot(), 1920, 1080)

    ROOT.gApplication.Run()
